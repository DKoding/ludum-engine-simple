package no.dkit.gamelib.xxxx.core.game.view.table;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import no.dkit.gamelib.xxxx.core.XXXX;

/**
 * Remember to update the tweener during rendering if you use this
 */
public abstract class StackTable extends NoImageTable {
    protected Stack stack;
    private int stackSize;

    public StackTable(boolean canBeClosed) {
        super(canBeClosed);
    }

    public StackTable() {
        super(false);
    }

    @Override
    protected void setupDimensions() {
        tableWidth = Math.round(screenWidth * .4f);
        tableHeight = Math.round(screenHeight * .9f);
        stackSize = tableWidth / 4;
        padding = stackSize / 20;
    }

    @Override
    protected void setupPosition() {
        tableXPos = (screenWidth - tableWidth);
        tableYPos = (screenHeight - tableHeight);
    }

    @Override
    protected Table getContentTable() {
        setupTextUI();
        setupStackUI();

        Table table = new Table(XXXX.skin);
        table.add(stack).size(stackSize).align(Align.top).expand();
        table.row();
        table.add(textLabel).pad(padding).align(Align.left | Align.top).width(tableWidth - padding);
        return table;
    }

    protected void setupStackUI() {
        stack = new Stack();
/*
        stack.setOrigin(Align.top);
        stack.setSize(stackSize, stackSize);
*/
    }

    protected void setupTextUI() {
        textLabel = new Label(text, XXXX.skin, "default");
        textLabel.setFontScale(1f);
        textLabel.setWrap(true);
        textLabel.setAlignment(Align.top | Align.left);
    }
}
