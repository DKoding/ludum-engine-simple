package no.dkit.gamelib.xxxx.core.game.view.table;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import no.dkit.gamelib.xxxx.core.XXXX;

/**
 * Remember to update the tweener during rendering if you use this
 */
public abstract class NoImageTable extends ControllableTable {
    protected Label textLabel;
    protected String text;

    public NoImageTable(boolean canBeClosed) {
        super(canBeClosed);
    }

    @Override
    protected Table getContentTable() {
        setupTextUI();

        Table table = new Table();
        table.add(textLabel).pad(padding).align(Align.left | Align.top).width(tableWidth - padding);

        return table;
    }

    protected void setupTextUI() {
        padding = 50;
        textLabel = new Label(text, XXXX.skin, "large");
        textLabel.setFontScale(1f);
        textLabel.setWrap(true);
        textLabel.setAlignment(Align.top | Align.left);
    }
}
