package no.dkit.gamelib.xxxx.core.game.view;

import aurelienribon.tweenengine.TweenAccessor;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class TweenActorAccessor implements TweenAccessor<Actor> {
    public static final int POSITION_X = 1;
    public static final int POSITION_Y = 2;
    public static final int POSITION_XY = 3;
    public static final int SIZE_X = 4;
    public static final int SIZE_Y = 5;
    public static final int SIZE_XY = 6;
    public static final int SCALE_XY = 7;
    public static final int ROTATION = 8;

    @Override
    public int getValues(Actor target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case POSITION_X:
                returnValues[0] = target.getX();
                return 1; // Number of values returned
            case POSITION_Y:
                returnValues[0] = target.getY();
                return 1; // Number of values returned
            case POSITION_XY:
                returnValues[0] = target.getX();
                returnValues[1] = target.getY();
                return 2; // Number of values returned
            case SIZE_X:
                returnValues[0] = target.getWidth();
                return 1; // Number of values returned
            case SIZE_Y:
                returnValues[0] = target.getHeight();
                return 1; // Number of values returned
            case SIZE_XY:
                returnValues[0] = target.getWidth();
                returnValues[1] = target.getHeight();
                return 2; // Number of values returned
            case SCALE_XY:
                returnValues[0] = target.getScaleX();
                returnValues[1] = target.getScaleY();
                return 2; // Number of values returned
            case ROTATION:
                returnValues[0] = target.getRotation();
                return 1; // Number of values returned
            default:
                return -1;
        }
    }

    @Override
    public void setValues(Actor target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case POSITION_X:
                target.setX(newValues[0]);
                break;
            case POSITION_Y:
                target.setY(newValues[0]);
                break;
            case POSITION_XY:
                target.setX(newValues[0]);
                target.setY(newValues[1]);
                break;
            case SIZE_X:
                target.setWidth(newValues[0]);
                break;
            case SIZE_Y:
                target.setHeight(newValues[0]);
                break;
            case SIZE_XY:
                target.setWidth(newValues[0]);
                target.setHeight(newValues[1]);
                break;
            case SCALE_XY:
                target.setScale(newValues[0], newValues[1]);
                break;
            case ROTATION:
                target.setRotation(newValues[0]);
            default:
        }
    }
}
