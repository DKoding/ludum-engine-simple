package no.dkit.gamelib.xxxx.core.game.transition;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;

import java.util.ArrayList;

public class TransitionScreen implements Screen {
    Screen current;
    Screen next;

    DoneCallback done;
    ChangeCallback change;

    int currentTransitionEffect;
    ArrayList<TransitionEffect> transitionEffects;

    public TransitionScreen(Screen current, Screen next, ChangeCallback change, DoneCallback done) {
        this.done = done;
        this.change = change;
        this.current = current;
        this.next = next;
        this.currentTransitionEffect = 0;

        transitionEffects = new ArrayList<TransitionEffect>();
        transitionEffects.add(new FadeOutEffect(500, Color.BLACK));
        transitionEffects.add(new FadeInEffect(500, Color.BLACK));
    }

    public void render(float delta) {
        transitionEffects.get(currentTransitionEffect).render(current, next);

        if (transitionEffects.get(currentTransitionEffect).isFinished()) {
            currentTransitionEffect++;
            if (currentTransitionEffect < transitionEffects.size()) {
                transitionEffects.get(currentTransitionEffect).init();
                change.onSwitch(current, next);
            } else {
                done.done(current, next);

            }
        }
    }

    public void resize(int width, int height) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void show() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void hide() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void pause() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void resume() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void dispose() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}