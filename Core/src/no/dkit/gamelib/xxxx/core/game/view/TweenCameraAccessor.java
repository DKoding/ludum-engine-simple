package no.dkit.gamelib.xxxx.core.game.view;

import aurelienribon.tweenengine.TweenAccessor;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class TweenCameraAccessor implements TweenAccessor<OrthographicCamera> {
    public static final int POSITION_X = 1;
    public static final int POSITION_Y = 2;
    public static final int POSITION_XY = 3;
    public static final int ZOOM = 4;
    public static final int ROTATION_DEG = 5;

    float prevRotationDeg = 0;
    float rotationDeg = 0;

    @Override
    public int getValues(OrthographicCamera target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case POSITION_X:
                returnValues[0] = target.position.x;
                return 1; // Number of values returned
            case POSITION_Y:
                returnValues[0] = target.position.y;
                return 1; // Number of values returned
            case POSITION_XY:
                returnValues[0] = target.position.x;
                returnValues[1] = target.position.y;
                return 2; // Number of values returned
            case ZOOM:
                returnValues[0] = target.zoom;
                return 1; // Number of values returned
            case ROTATION_DEG:
                returnValues[0] = rotationDeg;
                return 1; // Number of values returned
            default:
                return -1;
        }
    }

    @Override
    public void setValues(OrthographicCamera target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case POSITION_X:
                target.position.x = newValues[0];
                break;
            case POSITION_Y:
                target.position.y = newValues[0];
                break;
            case POSITION_XY:
                target.position.x = newValues[0];
                target.position.y = newValues[1];
                break;
            case ZOOM:
                target.zoom = newValues[0];
                break;
            case ROTATION_DEG: // Do the same for body accessor?
                rotationDeg = newValues[0];
                target.rotate(rotationDeg-prevRotationDeg);
                prevRotationDeg = rotationDeg;
                break;
            default:
        }

        target.update();
    }
}
