package no.dkit.gamelib.xxxx.core.game;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Expo;
import aurelienribon.tweenengine.equations.Linear;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import no.dkit.gamelib.xxxx.core.XXXX;
import no.dkit.gamelib.xxxx.core.game.view.TweenSpriteAccessor;

public class GameSprite extends Sprite {
    protected int currentRegion = 0;

    TextureRegion[] regions;

    public boolean creating = false;
    public boolean tweening = false;
    public boolean active = false;

    protected final Vector2 right;
    protected final Vector2 left;
    protected final Vector2 down;
    protected final Vector2 up;
    protected final Vector2 center;

    Color color = new Color();
    protected Tween pulseTween;

    private GameSprite(TextureRegion[] regions) {
        super(regions[0]);
        this.regions = regions;

        this.active = false;

        float centerX = Gdx.graphics.getWidth() / 2 - (getWidth() / 2f);
        float centerY = Gdx.graphics.getHeight() / 2 - (getHeight() / 2f);

        right = new Vector2(Gdx.graphics.getWidth() - getWidth(), centerY);
        left = new Vector2(0, centerY);
        down = new Vector2(centerX, 0);
        up = new Vector2(centerX, Gdx.graphics.getHeight() - getHeight());
        center = new Vector2(centerX, centerY);

        setPosition(center.x, center.y);

        color.set(Color.GRAY);
    }

    public GameSprite(TextureRegion[] regions, int position) {
        this(regions);

        switch (position) {
            case 0:
                onCreate(center);
                break;
            case 1:
                onCreate(up);
                break;
            case 2:
                onCreate(right);
                break;
            case 4:
                onCreate(down);
                break;
            case 8:
                onCreate(left);
                break;
        }
    }

    protected void onCreate(final Vector2 position) {
        if (creating || tweening) return;
        creating = true;
        tweening = true;

        setColor(Color.BLACK);

        final GameSprite instance = this;

/*
        if(instance instanceof Player)
            System.out.println("CREATING");
*/

        Timeline.createSequence()
                .push(Tween.set(this, TweenSpriteAccessor.POSITION_XY).target(position.x, position.y))
                .push(Tween.set(this, TweenSpriteAccessor.SCALE_XY).target(0, 0))
                .push(Tween.to(this, TweenSpriteAccessor.SCALE_XY, .5f).target(1, 1).ease(Expo.IN))
                .setCallback(new TweenCallback() {
                    @Override
                    public void onEvent(int type, BaseTween<?> source) {
/*
                        if(instance instanceof Player)
                            System.out.println("CREATED");
*/

                        creating = false;
                        tweening = false;
                        active = true;
                        setColor(color);
                        startPulse();
                    }
                })
                .start(XXXX.getTweener());
    }

    private void startPulse() {
        pulseTween = Tween.to(this, TweenSpriteAccessor.SCALE_XY, 1f / 2f).target(1.2f, 1.2f).ease(Expo.INOUT).repeatYoyo(-1, 0)
                .delay((Config.TIME_UNTIL_NEXT_BEAT / 1000f)).start(XXXX.getTweener());
    }

    protected void tweenTo(Vector2 position) {
        if (tweening) return;
        tweening = true;
        Timeline.createSequence()
                .push(
                        Tween.to(this, TweenSpriteAccessor.POSITION_XY, .2f).target(position.x, position.y).ease(Linear.INOUT)
                                .setCallback(new TweenCallback() {
                                    @Override
                                    public void onEvent(int type, BaseTween<?> source) {
                                        tweening = false;
                                    }
                                })
                )
                .start(XXXX.getTweener());
    }

    protected void updateRegion(int region) {
        currentRegion = region;
        setRegion(regions[currentRegion]);
    }

    @Override
    public void draw(Batch batch) {
        if (Config.TIME_UNTIL_NEXT_BEAT > 450 && Config.TIME_UNTIL_NEXT_BEAT < 550)
            setColor(Color.WHITE);
        else
            setColor(color);

        rotate(1);
        super.draw(batch);
    }
}
