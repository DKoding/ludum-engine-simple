package no.dkit.gamelib.xxxx.core.game.factory;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.utils.Pool;

public class ParticleEffectPool extends Pool<ParticleEffectPool.PooledEffect> {
    protected final ParticleEffect effect;
    protected final EffectFactory.EFFECT_TYPE type;

    public ParticleEffectPool(ParticleEffect effect, EffectFactory.EFFECT_TYPE type, int initialCapacity, int max) {
        super(initialCapacity, max);
        this.effect = effect;
        this.type = type;
    }

    protected PooledEffect newObject() {
        return new PooledEffect(effect);
    }

    public PooledEffect obtain() {
        PooledEffect effect = super.obtain();
        effect.reset();
        return effect;
    }

    public class PooledEffect extends ParticleEffect {
        PooledEffect(ParticleEffect effect) {
            super(effect);
        }

        @Override
        public void reset() {
            super.reset();
        }

        public void free() {
            ParticleEffectPool.this.free(this);
        }
    }
}
