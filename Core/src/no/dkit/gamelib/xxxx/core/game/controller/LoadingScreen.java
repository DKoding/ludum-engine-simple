package no.dkit.gamelib.xxxx.core.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import no.dkit.gamelib.xxxx.core.XXXX;
import no.dkit.gamelib.xxxx.core.game.factory.EffectFactory;
import no.dkit.gamelib.xxxx.core.game.factory.ResourceFactory;
import no.dkit.gamelib.xxxx.core.game.factory.TextFactory;
import no.dkit.gamelib.xxxx.core.game.transition.ChangeCallback;

public class LoadingScreen implements Screen, ChangeCallback {
    private SpriteBatch spriteBatch;
    private Texture progress;
    boolean loading = false;
    boolean finishedLoading = false;
    private float percentComplete;
    private GlyphLayout glyphLayout = new GlyphLayout();
    private final BitmapFont font;
    private final String text;
    private final float xPos;
    private final float yPos;
    private final XXXX.SCREEN to;

    public LoadingScreen(XXXX.SCREEN to) {
        this.to = to;
        finishedLoading = false;
        loading = false;
        spriteBatch = new SpriteBatch();
        progress = new Texture(Gdx.files.internal("images/loading.png"));

        EffectFactory.getInstance();

        font = TextFactory.getInstance().getFont();
        text = "Loading, please wait...";
        glyphLayout.setText(font, text);
        xPos = Gdx.graphics.getWidth() / 2 - (glyphLayout.width / 2);
        yPos = Gdx.graphics.getHeight() / 2 - (glyphLayout.height * 2);
    }

    public void render(float delta) {
        if (loading) {
            finishedLoading = ResourceFactory.getInstance().poll();

            Gdx.graphics.getGL20().glClearColor(0, 0, 0, 1);
            Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

            spriteBatch.begin();

            spriteBatch.setColor(Color.RED);
            spriteBatch.draw(progress,
                    Gdx.graphics.getWidth() / 2 - progress.getWidth() * 100 / 2,
                    Gdx.graphics.getHeight() / 2 - progress.getHeight() / 2,
                    progress.getWidth() * 100, progress.getHeight() / 3);
            spriteBatch.setColor(Color.GREEN);

            percentComplete = ResourceFactory.getInstance().getPercentComplete();

            spriteBatch.draw(progress,
                    Gdx.graphics.getWidth() / 2 - progress.getWidth() * 100 / 2,
                    Gdx.graphics.getHeight() / 2 - progress.getHeight() / 2,
                    progress.getWidth() * 100 * percentComplete, progress.getHeight() / 3);
            spriteBatch.setColor(Color.WHITE);
            spriteBatch.end();

            spriteBatch.begin();
            font.draw(spriteBatch, text, xPos, yPos);
            spriteBatch.end();
        }

        if (finishedLoading) {
            loading = false;
            if (to.equals(XXXX.SCREEN.NEWLEVEL))
                XXXX.changeScreen(XXXX.SCREEN.GAME);
            else if (to.equals(XXXX.SCREEN.INTRO))
                XXXX.changeScreen(XXXX.SCREEN.INTRO);
            else if (to.equals(XXXX.SCREEN.EXTRO))
                XXXX.changeScreen(XXXX.SCREEN.EXTRO);
        }
    }

    public void resize(int width, int height) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void show() {
        //
    }

    public void hide() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void pause() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void resume() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void dispose() {
    }

    @Override
    public void onSwitch(Screen oldScreen, Screen newScreen) {
        loading = true;
    }
}