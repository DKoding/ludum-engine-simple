package no.dkit.gamelib.xxxx.core.game.factory;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;
import no.dkit.gamelib.xxxx.core.XXXX;
import no.dkit.gamelib.xxxx.core.game.Config;

import java.util.HashMap;
import java.util.Map;

public class SoundFactory implements AssetErrorListener {
    static final String musicFormat = ".mp3";
    static final String soundFormat = ".wav";

    static final String soundpath = "sound/";
    static final String musicpath = "music/";

    static SoundFactory soundFactory;

    StringBuilder builder = new StringBuilder();

    public Music music;
    private long lastSoundPlay;
    private Sound currentSound;
    private Sound newSound;
    private long lastBeatRecording;

    public void heartbeat() {
        float position = -1f;

        if (music != null && music.isPlaying())
            position = music.getPosition();
        else {
            lastBeatRecording = System.currentTimeMillis();
            Config.TIME_UNTIL_NEXT_BEAT = 1000;
        }

        if (System.currentTimeMillis() > lastBeatRecording + 500 && position > -1f && position % 1f <= .05f) {
            lastBeatRecording = System.currentTimeMillis();
            SoundFactory.getInstance().playSound(SoundFactory.SOUND_TYPE.BEAT);
        } else {
            Config.TIME_UNTIL_NEXT_BEAT = System.currentTimeMillis() - lastBeatRecording;
        }
    }

    public enum SOUND_TYPE {
        HIT, MISS, MEGAHIT, SEMIHIT, BEAT
    }

    public enum MUSIC_TYPE {
        GAME
    }

    private static final Map<SOUND_TYPE, String> soundFileNames;
    private static final Map<MUSIC_TYPE, String> musicFileNames;

    AssetManager manager;

    static {
        musicFileNames = new HashMap<>();
        musicFileNames.put(MUSIC_TYPE.GAME, "game");

        soundFileNames = new HashMap<>();

        soundFileNames.put(SOUND_TYPE.HIT, "hit");
        soundFileNames.put(SOUND_TYPE.SEMIHIT, "semihit");
        soundFileNames.put(SOUND_TYPE.MISS, "miss");
        soundFileNames.put(SOUND_TYPE.MEGAHIT, "megahit");
        soundFileNames.put(SOUND_TYPE.BEAT, "beat");
    }

    public static SoundFactory getInstance() {
        if (soundFactory == null)
            soundFactory = new SoundFactory();

        return soundFactory;
    }

    protected SoundFactory() {
        MathUtils.random.setSeed(666);

        manager = new AssetManager();
        manager.setErrorListener(this);

        for (String fileName : soundFileNames.values()) {
            manager.load(soundpath + fileName + soundFormat, Sound.class);
        }

        for (String fileName : musicFileNames.values()) {
            manager.load(musicpath + fileName + musicFormat, Music.class);
        }
    }

    public void dispose() {
        manager.dispose();
        if (music != null)
            music.dispose();
        soundFactory = null;
    }

    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {

    }

    public void playMusic(MUSIC_TYPE type) {
        if (!Config.music || !XXXX.settings.isMusic()) return;

        if (music != null && music.isPlaying())
            music.stop();

        music = manager.get(musicpath + musicFileNames.get(type) + musicFormat, Music.class);

        music.setPosition(0);

        switch (type) {
            case GAME:
                music.setLooping(true);
                break;
        }

        System.out.println("Starting music at " + (System.currentTimeMillis() + Config.TIME_UNTIL_NEXT_BEAT));

        Tween.call(new TweenCallback() {
            @Override
            public void onEvent(int type, BaseTween<?> source) {
                music.play();
            }
        }).delay(Config.TIME_UNTIL_NEXT_BEAT / 1000f).start(XXXX.getTweener());
    }

    /**
     * Plays the sound. If the sound is already playing, it will be played again, concurrently.
     *
     * @param pitch the pitch multiplier, 1 == default, >1 == faster, <1 == slower, the value has to be between 0.5 and 2.0
     * @return the id of the sound instance if successful, or -1 on failure.
     */
    public void playSound(SOUND_TYPE type, float pitch) {
        if (!Config.sound) return;

        builder.setLength(0);
        builder.append(soundpath).append(soundFileNames.get(type)).append(soundFormat);
        newSound = manager.get(builder.toString(), Sound.class);
        if (newSound == currentSound && System.currentTimeMillis() - lastSoundPlay <= 100)
            return; // Prevent same sounds playing
        lastSoundPlay = System.currentTimeMillis();
        newSound.play(1f, pitch, 0f);
        currentSound = newSound;
    }

    /**
     * Plays the sound. If the sound is already playing, it will be played again, concurrently.
     *
     * @param pitch the pitch multiplier, 1 == default, >1 == faster, <1 == slower, the value has to be between 0.5 and 2.0
     * @return the id of the sound instance if successful, or -1 on failure.
     */
    public void playOneOf(SOUND_TYPE[] types, float pitch) {
        playSound(types[MathUtils.random(types.length - 1)], pitch);
    }

    public void playSound(SOUND_TYPE type) {
        playSound(type, 1f);
    }

    public void resumeMusic() {
        if (music != null && !music.isPlaying()) music.play();
    }

    public void stopMusic() {
        if (music != null && music.isPlaying()) music.stop();
    }

    public boolean poll() {
        return manager.update();
    }
}
