package no.dkit.gamelib.xxxx.core.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.utils.BufferUtils;

import java.nio.IntBuffer;

public class Decals {
    private FrameBuffer fbo;
    private TextureRegion fboRegion;

    public Decals() {
        IntBuffer maxDecalSize = BufferUtils.newIntBuffer(16);
        Gdx.gl20.glGetIntegerv(GL20.GL_MAX_TEXTURE_SIZE, maxDecalSize);
        final int i = maxDecalSize.get();
        createRenderBuffer(i);
    }

    private void createRenderBuffer(int maxSize) {
        final int width = Gdx.graphics.getWidth();
        final int height = Gdx.graphics.getHeight();

        if (width > maxSize || height > maxSize)
            throw new RuntimeException("Render buffer " + width + "x" + height + " could not be created, max size is " + maxSize);

        System.out.println("CREATING FRAMEBUFFER FOR " + width + ":" + height);

        fbo = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false);

        fboRegion = new TextureRegion(fbo.getColorBufferTexture());
        fboRegion.flip(false, true);
    }

    public void record() {
        fbo.begin();
    }

    public void end() {
        fbo.end();
    }

    public void render(SpriteBatch batch) {
        batch.draw(fboRegion, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    public void dispose() {
        fbo.dispose();
    }
}
