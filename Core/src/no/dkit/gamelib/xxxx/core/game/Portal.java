package no.dkit.gamelib.xxxx.core.game;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Bounce;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import no.dkit.gamelib.xxxx.core.XXXX;
import no.dkit.gamelib.xxxx.core.game.factory.EffectFactory;
import no.dkit.gamelib.xxxx.core.game.factory.SoundFactory;
import no.dkit.gamelib.xxxx.core.game.factory.TextFactory;
import no.dkit.gamelib.xxxx.core.game.view.TweenSpriteAccessor;

public class Portal extends GameSprite {

    public static final int MEGAHIT = 3;
    public static final int HIT = 2;
    public static final int SEMIHIT = 1;
    public static final int MISS = 0;

    public Portal(TextureRegion[] regions, int direction, int type, Color color) {
        super(regions, direction);
        currentRegion = type;
        updateRegion(currentRegion);
        this.color.set(color);
    }

    public int onCollission(Player player) {
        if (!active) return -1;

        if(pulseTween != null)
            pulseTween.kill();

        if (player.getColor().equals(this.getColor()) && player.currentRegion == currentRegion && (Config.TIME_UNTIL_NEXT_BEAT <= 50 || Config.TIME_UNTIL_NEXT_BEAT >= 950)) {
            onMegaHit();
            return MEGAHIT;
        }
        if (player.getColor().equals(this.getColor()) && player.currentRegion == currentRegion) {
            onHit();
            return HIT;
        } else if (player.getColor().equals(this.getColor()) || player.currentRegion == currentRegion) {
            onSemiHit();
            return SEMIHIT;
        } else {
            onMiss();
            return MISS;
        }
    }

    protected void onHit() {
        if (tweening || !active) return;

        active = false;
        tweening = true;

        TextFactory.getInstance().addScreenText("GREAT", Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, Color.GREEN);
        EffectFactory.getInstance().addUIEffect((int) (getX() + (getWidth() / 2f)), (int) (getY() + (getHeight() / 2)), EffectFactory.EFFECT_TYPE.ACHIEVE);
        SoundFactory.getInstance().playSound(SoundFactory.SOUND_TYPE.HIT);

        Timeline.createSequence()
                .push(Tween.to(this, TweenSpriteAccessor.SCALE_XY, 1f).target(0, 0).ease(Bounce.OUT)
                        .setCallback(new TweenCallback() {
                            @Override
                            public void onEvent(int type, BaseTween<?> source) {
                                tweening = false;
                            }
                        })
                )
                .start(XXXX.getTweener());
    }

    protected void onMegaHit() {
        if (tweening || !active) return;

        active = false;
        tweening = true;

        TextFactory.getInstance().addScreenText("AMAZING!!!", Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, Color.GOLDENROD);
        EffectFactory.getInstance().addUIEffect((int) (getX() + (getWidth() / 2f)), (int) (getY() + (getHeight() / 2)), EffectFactory.EFFECT_TYPE.ACHIEVE);
        SoundFactory.getInstance().playSound(SoundFactory.SOUND_TYPE.MEGAHIT);

        Timeline.createSequence()
                .push(Tween.to(this, TweenSpriteAccessor.SCALE_XY, 1f).target(0, 0).ease(Bounce.OUT)
                        .setCallback(new TweenCallback() {
                            @Override
                            public void onEvent(int type, BaseTween<?> source) {
                                tweening = false;
                            }
                        })
                )
                .start(XXXX.getTweener());
    }

    protected void onSemiHit() {
        if (tweening || !active) return;

        active = false;
        tweening = true;

        TextFactory.getInstance().addScreenText("GOOD", Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, Color.YELLOW);
        EffectFactory.getInstance().addUIEffect((int) (getX() + (getWidth() / 2f)), (int) (getY() + (getHeight() / 2)), EffectFactory.EFFECT_TYPE.ACHIEVE);
        SoundFactory.getInstance().playSound(SoundFactory.SOUND_TYPE.SEMIHIT);

        Timeline.createSequence()
                .push(Tween.to(this, TweenSpriteAccessor.SCALE_XY, 1f).target(0, 0).ease(Bounce.OUT)
                        .setCallback(new TweenCallback() {
                            @Override
                            public void onEvent(int type, BaseTween<?> source) {
                                tweening = false;
                            }
                        })
                )
                .start(XXXX.getTweener());
    }

    protected void onMiss() {
        if (tweening || !active) return;

        active = false;
        tweening = true;

        TextFactory.getInstance().addScreenText("Ouch...", Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, Color.RED);
        EffectFactory.getInstance().addUIEffect((int) (getX() + (getWidth() / 2f)), (int) (getY() + (getHeight() / 2)), EffectFactory.EFFECT_TYPE.BIGEXPLOSION);
        SoundFactory.getInstance().playSound(SoundFactory.SOUND_TYPE.MISS);

        Timeline.createSequence()
                .push(Tween.to(this, TweenSpriteAccessor.SCALE_XY, 1f).target(0, 0).ease(Bounce.OUT)
                        .setCallback(new TweenCallback() {
                            @Override
                            public void onEvent(int type, BaseTween<?> source) {
                                tweening = false;
                            }
                        })
                )
                .start(XXXX.getTweener());
    }

    public void stopTweener() {
        if (pulseTween != null)
            pulseTween.kill();
    }
}
