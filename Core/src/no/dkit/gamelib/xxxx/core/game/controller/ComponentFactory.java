package no.dkit.gamelib.xxxx.core.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import no.dkit.gamelib.xxxx.core.XXXX;

public class ComponentFactory {
    public static final String TRANSPARENT = "transparent";
    public static final String TOGGLE = "toggle";
    public static final String DEFAULT = "default";

    public static final int BUTTONSIZE = Gdx.graphics.getHeight() / 5;

    public static Button createTransparentButton(TextureRegion right, ImageListener.ListenerActions listener) {
        final Image image = new Image(right);
        Button button = new Button(image, XXXX.skin, TRANSPARENT);
        button.addListener(new ImageListener(image, listener));
        return button;
    }
}
