package no.dkit.gamelib.xxxx.core.game.transition;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class CircleInEffect extends TransitionEffect {

    Color color = new Color();
    ShapeRenderer spriteBatch;

    public CircleInEffect(long duration, Color color) {
        super(duration);
        this.color = color;

        spriteBatch = new ShapeRenderer();
    }

    @Override
    public void render(Screen current, Screen next) {
        next.render(Gdx.graphics.getDeltaTime());

        Gdx.gl20.glEnable(GL20.GL_BLEND);
        Gdx.gl20.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        spriteBatch.begin(ShapeRenderer.ShapeType.Filled);
        spriteBatch.setColor(0, 0, 0, 1f - getDelta());
        spriteBatch.circle(Gdx.graphics.getWidth()/2f, Gdx.graphics.getHeight()/2f, Gdx.graphics.getWidth()/2f * getDelta());
        spriteBatch.end();
        Gdx.gl20.glDisable(GL20.GL_BLEND);
    }
}