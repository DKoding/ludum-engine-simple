package no.dkit.gamelib.xxxx.core.game.transition;

import com.badlogic.gdx.Screen;

public interface DoneCallback {
    void done(Screen oldScreen, Screen newScreen);
}
