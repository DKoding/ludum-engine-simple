package no.dkit.gamelib.xxxx.core.game;

import com.badlogic.gdx.Gdx;

public class Config {
    public static final float CONTROLLER_DEADZONE = .1f;
    public static final Integer MAX_NUMBER_OF_PLAYERS = 1;
    public static final boolean DEBUG = false;
    public static final float DEGUG_THRESHOLD_MILLIS = 10;
    public static final int CONTROL_CHECK_MILLIS = 25;
    public static final int LEVEL_LENGTH = 7;
    public static final int FIRST_LEVEL = 15;
    public static final int SECOND_LEVEL = FIRST_LEVEL + LEVEL_LENGTH;
    public static final int THIRD_LEVEL = SECOND_LEVEL + LEVEL_LENGTH;
    public static final int FOURTH_LEVEL = THIRD_LEVEL + LEVEL_LENGTH;
    public static final int FIFTH_LEVEL = FOURTH_LEVEL + LEVEL_LENGTH;
    public static final int SIXTH_LEVEL = FIFTH_LEVEL + LEVEL_LENGTH;
    public static final int SEVENTH_LEVEL = SIXTH_LEVEL + LEVEL_LENGTH;
    public static final int EIGHTHT_LEVEL = SEVENTH_LEVEL + LEVEL_LENGTH;
    public static final int NINTH_LEVEL = EIGHTHT_LEVEL + LEVEL_LENGTH;
    public static final int WIN = NINTH_LEVEL + LEVEL_LENGTH * 2;
    public static final int NUM_COLORS = 5;
    public static final int NUM_TYPES = 5;
    public static final long LEVEL_TIME = 10000;
    public static final int TEXT_DELAY = 1000;
    public static final int TUTORIAL_DELAY = 1000 * 3600;
    public static boolean PAUSED = false;
    public static boolean music = true;
    public static boolean sound = true;

    public static final int TEXT_SIZE = 25;
    public static final float TEXT_SCROLL_SPEED = TEXT_SIZE / 20f;
    public static final int TEXT_TIME_TO_LIVE = 10000;           // 10 seconds

    public static long TIME_UNTIL_NEXT_BEAT = 0;
}
