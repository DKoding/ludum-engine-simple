package no.dkit.gamelib.xxxx.core.game.factory;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.PooledLinkedList;

public class EffectFactory {

    public static final int MAX_NUMBER_OF_SAME_EFFECT = 25;

    public enum DRAW_LAYER {UI, DECAL}

    public enum EFFECT_TYPE {
        BIGEXPLOSION, ACHIEVE, TRACK
    }

    static EffectFactory instance = null;
    private static World world;

    static AssetManager manager;

    ParticleEffectLoader particleEffectLoader;

    ArrayMap<EFFECT_TYPE, ParticleEffect> allEffectBlueprints = new ArrayMap<EFFECT_TYPE, ParticleEffect>();

    PooledLinkedList<ParticleEffectPool.PooledEffect> particleEffectsDecal = new PooledLinkedList<>(100);
    PooledLinkedList<ParticleEffectPool.PooledEffect> particleEffectsUI = new PooledLinkedList<>(100);

    ArrayMap<EFFECT_TYPE, ParticleEffectPool> effectPools = new ArrayMap();

    public static void create(World world) {
        if (instance == null || EffectFactory.world != world)
            instance = new EffectFactory(world);
        else
            throw new RuntimeException("ALREADY CREATED!");
    }

    public static EffectFactory getInstance() {
        if (instance == null)
            instance = new EffectFactory();

        return instance;
    }

    private EffectFactory() {
        this(null);
    }

    private EffectFactory(World world) {
        EffectFactory.world = world;

        manager = new AssetManager();

        particleEffectLoader = new ParticleEffectLoader(new InternalFileHandleResolver());

        TextureAtlas atlas = ResourceFactory.getInstance().getParticleAtlas();

        allEffectBlueprints.put(EFFECT_TYPE.BIGEXPLOSION, particleEffectLoader.load("effect/bigexplosion.txt", atlas));
        allEffectBlueprints.put(EFFECT_TYPE.ACHIEVE, particleEffectLoader.load("effect/achieve.txt", atlas));
        allEffectBlueprints.put(EFFECT_TYPE.TRACK, particleEffectLoader.load("effect/track.txt", atlas));

        ParticleEffect effect;

        for (EFFECT_TYPE effectType : allEffectBlueprints.keys()) {
            effect = allEffectBlueprints.get(effectType);
            effect.setEmittersCleanUpBlendFunction(false);
            effectPools.put(effectType, new ParticleEffectPool(effect, effectType, MAX_NUMBER_OF_SAME_EFFECT, Integer.MAX_VALUE));
        }
    }

    public ParticleEffect addDecalEffect(int x, int y, EFFECT_TYPE type) {
        return addEffect(x, y, type, DRAW_LAYER.DECAL);
    }

    public ParticleEffect addUIEffect(int x, int y, EFFECT_TYPE type) {
        return addEffect(x, y, type, DRAW_LAYER.UI);
    }

    public ParticleEffect addUIEffect(int x, int y, EFFECT_TYPE type, float angleDeg) {
        final ParticleEffect particleEffect = addEffect(x, y, type, DRAW_LAYER.UI);
        particleEffect.getEmitters().first().getAngle().setHighMin(-20 + angleDeg);
        particleEffect.getEmitters().first().getAngle().setHighMax(20 + angleDeg);
        return particleEffect;
    }

    public ParticleEffect addDecalEffect(int x, int y, EFFECT_TYPE type, float angleDeg) {
        final ParticleEffect particleEffect = addEffect(x, y, type, DRAW_LAYER.DECAL);
        particleEffect.getEmitters().first().getAngle().setHighMin(-20 + angleDeg);
        particleEffect.getEmitters().first().getAngle().setHighMax(20 + angleDeg);
        return particleEffect;
    }

    public ParticleEffect addEffect(int x, int y, EFFECT_TYPE type, DRAW_LAYER layer) {
        ParticleEffectPool.PooledEffect particleEffect = effectPools.get(type).obtain();
        particleEffect.setPosition(x, y);
        particleEffect.start();

        addToLayer(particleEffect, layer);

        return particleEffect;
    }

    public void rotateEffect(ParticleEffect particleEffect, float firingAngleDeg, float arc) {
        final Array<ParticleEmitter> emitters = particleEffect.getEmitters();
        for (ParticleEmitter emitter : emitters) {
            ParticleEmitter.ScaledNumericValue val = emitter.getAngle();
            float h1 = firingAngleDeg + arc;
            float h2 = firingAngleDeg - arc;
            val.setHigh(h1, h2);
            val.setLow(firingAngleDeg);
        }
    }

    public void rotateEffect(ParticleEffect particleEffect, float firingAngleDeg) {
        final Array<ParticleEmitter> emitters = particleEffect.getEmitters();
        for (ParticleEmitter emitter : emitters) {
            ParticleEmitter.ScaledNumericValue val = emitter.getAngle();
            float h1 = firingAngleDeg + val.getHighMin();
            float h2 = firingAngleDeg - val.getHighMax();
            val.setHigh(h1, h2);
            val.setLow(firingAngleDeg);
        }
    }

    private void addToLayer(ParticleEffectPool.PooledEffect particleEffect, DRAW_LAYER layer) {
        if (layer == DRAW_LAYER.DECAL)
            particleEffectsDecal.add(particleEffect);
        else if (layer == DRAW_LAYER.UI)
            particleEffectsUI.add(particleEffect);
    }

    public void stopEffect(ParticleEffect effect) {
        if (effect != null)
            effect.allowCompletion();
    }

    public void update(float deltaTime) {
        updateEffects(deltaTime, particleEffectsDecal);
        updateEffects(deltaTime, particleEffectsUI);
    }

    private void updateEffects(float deltaTime, PooledLinkedList<ParticleEffectPool.PooledEffect> effects) {
        effects.iter();
        ParticleEffectPool.PooledEffect effect = effects.next();

        while (effect != null) {
            if (effect.isComplete()) {
                effect.free();
                effects.remove();
            } else {
                effect.update(deltaTime);
            }
            effect = effects.next();
        }
    }

    public void drawEffects(SpriteBatch spriteBatch, DRAW_LAYER layer) {
        if (layer == DRAW_LAYER.DECAL && particleEffectsDecal.size() > 0)
            drawEffects(spriteBatch, particleEffectsDecal);
        else if (layer == DRAW_LAYER.UI && particleEffectsUI.size() > 0)
            drawEffects(spriteBatch, particleEffectsUI);

        spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);   // Because effects does not clean up after themselves
    }

    private void drawEffects(SpriteBatch spriteBatch, PooledLinkedList<ParticleEffectPool.PooledEffect> effects) {
        effects.iter();
        ParticleEffectPool.PooledEffect effect = effects.next();

        while (effect != null) {
            effect.draw(spriteBatch);
            effect = effects.next();
        }
/*
        if (Config.DEBUGTEXT)
            System.out.println("Drew " + effects.size() + " effects");
*/
    }

    public void dispose() {
        for (ParticleEffect effect : allEffectBlueprints.values()) {
            effect.dispose();
        }

        manager.dispose();
        manager = null;
        instance = null;

        System.out.println(this.getClass().getName() + " disposed");
    }

    private class ParticleEffectLoader extends SynchronousAssetLoader<ParticleEffect, ParticleEffectLoader.ParticleEffectParameter> {

        public ParticleEffectLoader(FileHandleResolver resolver) {
            super(resolver);
        }

        @Override
        public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, ParticleEffectParameter parameter) {
            return null;
        }

        @Override
        public ParticleEffect load(AssetManager assetManager, String fileName, FileHandle file, ParticleEffectParameter parameter) {
            ParticleEffect effect = new ParticleEffect();
            FileHandle imgDir = file.parent();
            effect.load(file, imgDir);
            return effect;
        }

        public ParticleEffect load(String fileName, TextureAtlas atlas) {
            ParticleEffect effect = new ParticleEffect();
            FileHandle file = resolve(fileName);
            effect.load(file, atlas);
            return effect;
        }

        public class ParticleEffectParameter extends AssetLoaderParameters<ParticleEffect> {
            public ParticleEffectParameter() {
            }
        }
    }
}
