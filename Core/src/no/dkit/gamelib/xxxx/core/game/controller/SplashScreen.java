package no.dkit.gamelib.xxxx.core.game.controller;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Expo;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.bitfire.postprocessing.filters.CrtScreen;
import no.dkit.gamelib.xxxx.core.XXXX;
import no.dkit.gamelib.xxxx.core.game.Config;
import no.dkit.gamelib.xxxx.core.game.factory.EffectFactory;
import no.dkit.gamelib.xxxx.core.game.factory.ResourceFactory;
import no.dkit.gamelib.xxxx.core.game.factory.SoundFactory;
import no.dkit.gamelib.xxxx.core.game.factory.TextFactory;
import no.dkit.gamelib.xxxx.core.game.view.TweenActorAccessor;

public class SplashScreen implements Screen {
    private SpriteBatch spriteBatch;
    boolean ready = false;
    boolean ready2 = false;
    private boolean introDone = false;
    OrthographicCamera camera;
    private final Stage stage;

    public SplashScreen() {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(false);

        spriteBatch = new SpriteBatch();
        spriteBatch.setProjectionMatrix(camera.combined);

        ResourceFactory.getInstance();
        SoundFactory.getInstance();

        final Texture logo = new Texture(Gdx.files.internal("images/logo.png"));
        logo.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        TextureRegion splashImage = new TextureRegion(logo);
        final Image logoImage = createLogoImage(splashImage);
        stage = new Stage(new ScreenViewport(camera));

        stage.addActor(logoImage);

        Timeline.createSequence()
                .setCallback(new TweenCallback() {
                    @Override
                    public void onEvent(int type, BaseTween<?> source) {
                        introDone = true;
                    }
                })
                .push(Tween.to(logoImage, TweenActorAccessor.SCALE_XY, .5f).target(1.1f, 1.1f).ease(Expo.OUT))
                .push(Tween.to(logoImage, TweenActorAccessor.SCALE_XY, .5f).target(0f, 0f).ease(Expo.IN))
                .start(XXXX.getTweener());
    }

    public void render(float delta) {
        XXXX.getTweener().update(delta);

        ready = ResourceFactory.getInstance().poll();
        ready2 = SoundFactory.getInstance().poll();

        Gdx.graphics.getGL20().glClearColor(1, 1, 1, 1);
        Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        spriteBatch.begin();
        stage.act();
        stage.draw();
        spriteBatch.end();

        if (ready && ready2 && introDone) {
            if (Config.DEBUG) {
                XXXX.changeScreen(XXXX.SCREEN.NEWLEVEL);
            } else {
                XXXX.changeScreen(XXXX.SCREEN.STARTMENU);
            }
            ready = false;
            ready2 = false;
        }
    }

    public void resize(int width, int height) {
        camera.viewportWidth = width;
        camera.viewportHeight = height;
        camera.update();
        spriteBatch.setProjectionMatrix(camera.combined);
    }

    private Image createLogoImage(TextureRegion logo) {
        Image imageActor = new Image(logo);
        imageActor.setSize(logo.getRegionWidth() > Gdx.graphics.getWidth() ? Gdx.graphics.getWidth() : logo.getRegionWidth(),
                logo.getRegionHeight() > Gdx.graphics.getHeight() / 2f ? Gdx.graphics.getHeight() / 2f : logo.getRegionHeight());
        imageActor.setOrigin(Align.center);
        imageActor.setPosition((Gdx.graphics.getWidth() - imageActor.getWidth()) / 2f, (Gdx.graphics.getHeight() - imageActor.getHeight()) / 2f);
        return imageActor;
    }

    public void show() {
        //
    }

    public void hide() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void pause() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void resume() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void dispose() {

    }
}