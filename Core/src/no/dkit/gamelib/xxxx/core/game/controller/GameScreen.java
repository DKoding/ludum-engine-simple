package no.dkit.gamelib.xxxx.core.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import no.dkit.gamelib.xxxx.core.XXXX;
import no.dkit.gamelib.xxxx.core.game.Config;
import no.dkit.gamelib.xxxx.core.game.Player;
import no.dkit.gamelib.xxxx.core.game.Portal;
import no.dkit.gamelib.xxxx.core.game.factory.EffectFactory;
import no.dkit.gamelib.xxxx.core.game.factory.ResourceFactory;
import no.dkit.gamelib.xxxx.core.game.factory.TextFactory;

import static no.dkit.gamelib.xxxx.core.game.factory.ResourceFactory.UI;

public class GameScreen extends AbstractScreen {
    Color[] colors;

    long startProgress;
    long textDelay;
    long tutorialDelay;
    long progressDelay;
    long waitTime;

    int progress = 0;

    int totalHits;
    int numPortals;

    Player player;
    Array<Portal> portals;
    Array<Sprite> arrowSprites;

    private long lastControlCheck;
    private long lastCollissionCheck;
    private TextureRegion[] regions;
    private int[] type = new int[4];
    private Color[] color = new Color[4];

    private boolean lost = false;
    private boolean won = false;
    private long endTime;
    private boolean showTutorialArrows = false;
    private boolean showTimer = false;
    private int portalPositions;
    private final TextureAtlas.AtlasRegion arrowTexture;
    private Sprite up;
    private Sprite right;
    private Sprite down;
    private Sprite left;

    StringBuilder scoreBuilder = new StringBuilder();
    StringBuilder multiplierBuilder = new StringBuilder();
    StringBuilder timeBuilder = new StringBuilder();
    Label scoreLead = new Label("SCORE", XXXX.skin, "large");
    Label scoreLabel = new Label("", XXXX.skin, "large");

    Label multiplierLead = new Label("MUL X", XXXX.skin, "large");
    Label multiplierLabel = new Label("", XXXX.skin, "large");

    Label timeLead = new Label("TIME", XXXX.skin, "large");
    Label timelabel = new Label("", XXXX.skin, "large");
    private int score;
    private int multiplier;
    private long timeLeft;

    public GameScreen() {
        MathUtils.random.setSeed(666);

        configureColors();

        regions = ResourceFactory.getInstance().getRegions();

        arrowTexture = ResourceFactory.getInstance().getImage(UI, "arrow");

        TextFactory.getInstance().clear();

        progress = 0;
        textDelay = Config.TEXT_DELAY;
        progressDelay = Config.LEVEL_TIME;
        tutorialDelay = Config.TUTORIAL_DELAY;
        waitTime = textDelay;
        startProgress = System.currentTimeMillis();
        score = 0;
        multiplier = 1;
        showTimer = false;
        timelabel.setVisible(showTimer);
        timeLead.setVisible(showTimer);

        resetPlayer();
        resetPortals();

        Table helpTable = new Table(XXXX.skin);
        helpTable.defaults();

        for (int i = 0; i < regions.length; i++) {
            final Image actor = new Image(regions[i]);
            actor.setColor(colors[i]);
            helpTable.add(actor).size(Gdx.graphics.getHeight() / 10).pad(Gdx.graphics.getHeight() / 40);
        }

        helpTable.setOrigin(Align.center | Align.top);
        helpTable.pack();
//        helpTable.setWidth(Gdx.graphics.getWidth() / 2);
        helpTable.setPosition(Gdx.graphics.getWidth() / 2 - helpTable.getWidth() / 2, 0);

        scoreLead.setColor(Color.BLACK);
        scoreLabel.setColor(Color.BLACK);
        multiplierLead.setColor(Color.BLACK);
        multiplierLabel.setColor(Color.BLACK);
        timeLead.setColor(Color.BLACK);
        timelabel.setColor(Color.BLACK);

        Table scoreTable = new Table(XXXX.skin);
        scoreTable.defaults();
        scoreTable.add(scoreLead).width(Gdx.graphics.getWidth() / 6);
        scoreTable.add(scoreLabel).width(Gdx.graphics.getWidth() / 6);
        scoreTable.add(multiplierLead).width(Gdx.graphics.getWidth() / 6);
        scoreTable.add(multiplierLabel).width(Gdx.graphics.getWidth() / 6);
        scoreTable.add(timeLead).width(Gdx.graphics.getWidth() / 6);
        scoreTable.add(timelabel).width(Gdx.graphics.getWidth() / 6);
        scoreTable.setOrigin(Align.center | Align.bottom);
        scoreTable.pack();
//        helpTable.setWidth(Gdx.graphics.getWidth() / 2);
        scoreTable.setPosition(Gdx.graphics.getWidth() / 2 - scoreTable.getWidth() / 2, Gdx.graphics.getHeight() - scoreTable.getHeight());

        setupArrowSprites();

        stage.addActor(helpTable);
        stage.addActor(scoreTable);
    }

    private void configureColors() {
        colors = new Color[Config.NUM_COLORS];

        colors[0] = Color.RED;
        colors[1] = Color.GREEN;
        colors[2] = Color.BLUE;
        colors[3] = Color.CYAN;
        colors[4] = Color.MAGENTA;
    }

    private void resetPortals() {
        portals = new Array<>();
        arrowSprites = new Array<>();
    }

    private void resetPlayer() {
        won = false;
        lost = false;

        if (player != null) {
            player.dispose();
            player = null;
        }

        player = new Player(regions, colors);
        player.setOriginCenter();
    }

    protected void updateView(float delta) {
        System.out.println("active " + player.active);
        System.out.println("creating " + player.creating);
        System.out.println("tweening " + player.tweening);

        updatePostProcessors();

        postProcessor.capture();

        Gdx.graphics.getGL20().glClearColor(1, 1, 1, 1);
        Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        //background.renderFullScreen(spriteBatch);

        spriteBatch.begin();
        drawMovingObjects();
        drawPlayer();
        drawTutorialArrows();

        spriteBatch.end();

        stage.draw();

        postProcessor.render();

        spriteBatch.begin();
        spriteBatch.enableBlending();

        if (player.tweening)
            EffectFactory.getInstance().drawEffects(spriteBatch, EffectFactory.DRAW_LAYER.DECAL);

        EffectFactory.getInstance().drawEffects(spriteBatch, EffectFactory.DRAW_LAYER.UI);
        drawText();
        spriteBatch.end();
    }

    protected void updatePostProcessors() {
        final float rnd = Math.abs(Config.TIME_UNTIL_NEXT_BEAT);
        final float forward = 1.5f - MathUtils.clamp(rnd / 1000f, .5f, 1.5f);
        final float back = 2f - MathUtils.clamp(rnd / 1000f, 0f, 2f);

        bloom.setBaseIntesity(2f);
        bloom.setBloomIntesity(1.3f);
        bloom.setBaseSaturation(1f);
        bloom.setBloomSaturation(1f);
        vignette.setSaturation(1.8f);
        vignette.setSaturationMul(forward);
        vignette.setIntensity(1f);
        vignette.setLutIntensity(1f);
    }

    private void drawTutorialArrows() {
        if (showTutorialArrows)
            for (Sprite arrowSprite : arrowSprites) {
                arrowSprite.draw(spriteBatch);
            }
    }

    private void drawMovingObjects() {
        for (Portal portal : portals) {
            portal.draw(spriteBatch);
        }
    }

    private void drawPlayer() {
        player.draw(spriteBatch);
    }

    private void drawText() {
        TextFactory.getInstance().draw(spriteBatch);
    }

    protected void updateModel(float delta) {
        stage.act();

        scoreBuilder.setLength(0);
        scoreBuilder.append(score);
        scoreLabel.setText(scoreBuilder);

        multiplierBuilder.setLength(0);
        multiplierBuilder.append(multiplier);
        multiplierLabel.setText(multiplierBuilder);

        timeBuilder.setLength(0);
        timeLeft = (startProgress + waitTime) - System.currentTimeMillis();
        timeBuilder.append(timeLeft);
        timelabel.setText(timeBuilder);

        periodicallyParseInput();

        TextFactory.getInstance().update(0, 0);
        EffectFactory.getInstance().update(delta);

        if (lost) {
            if (System.currentTimeMillis() > endTime + 1000) {
                XXXX.gameState = XXXX.GAME_STATE.CLICK_TO_CONTINUE;
                endTime = System.currentTimeMillis();
                TextFactory.getInstance().addText("Space to retry, backspace for menu...");
            }
            return;
        }

        if (won) {
            if (System.currentTimeMillis() > endTime + 1000) {
                XXXX.gameState = XXXX.GAME_STATE.CLICK_TO_CONTINUE;
                endTime = System.currentTimeMillis();
                for (int i = 0; i < 10; i++) {
                    EffectFactory.getInstance().addEffect(MathUtils.random(Gdx.graphics.getWidth()), MathUtils.random(Gdx.graphics.getHeight()), EffectFactory.EFFECT_TYPE.ACHIEVE, EffectFactory.DRAW_LAYER.UI);
                    cameraEffect();
                }
            }
            return;
        }

        checkForCollissions();

        if (numPortals > 0 && totalHits == numPortals) {
            numPortals = 0;
            totalHits = 0;
            resetPlayer();

            if (showTimer) {
                score += timeLeft;
                TextFactory.getInstance().addEvenMoreText("+" + timeLeft);
            }

            clearPortalsAndArrows();
            progress();
        } else if (System.currentTimeMillis() > (startProgress + waitTime)) {
            clearPortalsAndArrows();

            if (totalHits < numPortals) {
                TextFactory.getInstance().addText("TOO SLOW - YOU LOSE!");
                onLost();
            } else
                progress();
        }
    }

    private void checkForCollissions() {
        for (Portal portal : portals) {
            if (!portal.active) continue;

            if (MathUtils.isEqual(portal.getX(), player.getX(), Gdx.graphics.getHeight() / 20f) && MathUtils.isEqual(portal.getY(), player.getY(), Gdx.graphics.getHeight() / 20f)) {
                final int result = portal.onCollission(player);

                if (result == Portal.MISS) {
                    TextFactory.getInstance().addScreenText("YOU CRASHED - YOU LOSE!", Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, Color.RED);
                    onLost();
                } else {
                    totalHits++;

                    if (result == Portal.MEGAHIT) {
                        multiplier++;
                        cameraEffect();
                    }

                    if (result == Portal.SEMIHIT && multiplier > 1)
                        multiplier--;

                    score += 1000 * multiplier;

                    TextFactory.getInstance().addMoreText("+" + (1000 * multiplier));
                }
            }
        }
    }

    private void onLost() {
        player.dispose();
        EffectFactory.getInstance().addDecalEffect((int) (player.getX() + (player.getWidth() / 2f)), (int) (player.getY() + (player.getHeight() / 2)), EffectFactory.EFFECT_TYPE.BIGEXPLOSION);

        showTotalScore();

        lost = true;
        endTime = System.currentTimeMillis();

        showTimer = false;
        timelabel.setVisible(showTimer);
        timeLead.setVisible(showTimer);
    }

    private void showTotalScore() {
        TextFactory.getInstance().addMoreText("TOTAL SCORE: " + score);

        if (score > XXXX.HIGHSCORE) {
            XXXX.HIGHSCORE = score;
            TextFactory.getInstance().addEvenMoreText("YOU HAVE A NEW HIGHSCORE");
        }

        cameraEffect();
    }

    private void periodicallyParseInput() {
        if (System.currentTimeMillis() > lastControlCheck + Config.CONTROL_CHECK_MILLIS) {
            lastControlCheck = System.currentTimeMillis();
            player.parseControllerInput(getInput(0));
        }
    }

    protected void onStart() {

    }

    private void progress() {
        totalHits = 0;
        numPortals = 0;

        startProgress = System.currentTimeMillis();

        if (progress == 0) {
            TextFactory.getInstance().addText("SHAPEMATCH BEAT JAM");
        } else if (progress == 1) {
            TextFactory.getInstance().addText("Move the center shape to matching shapes");
        } else if (progress == 2) {
            TextFactory.getInstance().addText("One of color and shape must match");
        } else if (progress == 3) {
            TextFactory.getInstance().addText("Change shape with 1-2-3-4-5");
        } else if (progress == 4) {
            TextFactory.getInstance().addText("Change color with q-w-e-r-t");
        } else if (progress == 5) {
            TextFactory.getInstance().addText("Move with arrows up, down, left, right");
            showTutorialArrows = true;
            arrowSprites.add(up);
            arrowSprites.add(down);
            arrowSprites.add(left);
            arrowSprites.add(right);
        } else if (progress > 5 && progress < 8) {
            arrowSprites.add(up);
            arrowSprites.add(down);
            arrowSprites.add(left);
            arrowSprites.add(right);
        } else if (progress == 8) {
            TextFactory.getInstance().addText("Try to join shapes in time with the beat!");
        } else if (progress == 9) {
            TextFactory.getInstance().addText("Let's warm up...");
        } else if (progress < Config.FIRST_LEVEL) {
            waitTime = tutorialDelay;
            addPortals(progress);
        } else if (progress == Config.FIRST_LEVEL) {
            waitTime = textDelay;
            TextFactory.getInstance().addText("Okay, training over - START!");
            showTutorialArrows = false;
        } else if (progress == Config.FIRST_LEVEL + 1) {
            TextFactory.getInstance().addText("Let's add a timer to make it interesting...");
            showTimer = true;
            timelabel.setVisible(showTimer);
            timeLead.setVisible(showTimer);
        } else if (progress == Config.SECOND_LEVEL) {
            TextFactory.getInstance().addText("Too easy, huh? Let's kick it up a notch...");
        } else if (progress == Config.THIRD_LEVEL) {
            TextFactory.getInstance().addText("Oh yeah!? What about this!");
        } else if (progress == Config.FOURTH_LEVEL) {
            TextFactory.getInstance().addText("Not enough for you? Okay...");
        } else if (progress == Config.FIFTH_LEVEL) {
            TextFactory.getInstance().addText("How about THIS!");
        } else if (progress == Config.SIXTH_LEVEL) {
            TextFactory.getInstance().addText("Feeling confident?");
        } else if (progress == Config.SEVENTH_LEVEL) {
            TextFactory.getInstance().addText("Let's turn that smile upside down...");
        } else if (progress == Config.EIGHTHT_LEVEL) {
            TextFactory.getInstance().addText("I'm starting to lose patience...");
        } else if (progress == Config.NINTH_LEVEL) {
            TextFactory.getInstance().addText("No holds barred! FULL SPEED!");
        } else if (progress == Config.WIN) {
            won();
        }

        if (progress > Config.FIRST_LEVEL + 1) {
            waitTime = progressDelay;
            addPortals(progress);
        }

        if (progress >= Config.NINTH_LEVEL)
            cameraEffect();

        numPortals = portals.size;
        progress++;
    }

    private void won() {
        won = true;
        showTimer = false;
        timelabel.setVisible(showTimer);
        timeLead.setVisible(showTimer);
        TextFactory.getInstance().addText("Congratulations! YOU DID IT!");
        showTotalScore();
        XXXX.gameState = XXXX.GAME_STATE.CLICK_TO_CONTINUE;
    }

    private void addPortals(int progress) {
        getTypes(progress);
        getColors(progress);

        portalPositions = MathUtils.random(Math.min((int) (progress / 3f), 15), 15);

        // Too lazy to figure out better algorhitm, could probably be done in 3 lines
        switch (portalPositions) {
            case 1:
                portals.add(new Portal(regions, 1, type[0], color[0]));
                arrowSprites.add(up);
                break;
            case 2:
                portals.add(new Portal(regions, 2, type[0], color[0]));
                arrowSprites.add(right);
                break;
            case 3:
                portals.add(new Portal(regions, 1, type[0], color[0]));
                portals.add(new Portal(regions, 2, type[1], color[1]));
                arrowSprites.add(up);
                arrowSprites.add(right);
                break;
            case 4:
                portals.add(new Portal(regions, 4, type[0], color[0]));
                arrowSprites.add(down);
                break;
            case 5:
                portals.add(new Portal(regions, 1, type[0], color[0]));
                portals.add(new Portal(regions, 4, type[1], color[1]));
                arrowSprites.add(up);
                arrowSprites.add(down);
                break;
            case 6:
                portals.add(new Portal(regions, 2, type[0], color[0]));
                portals.add(new Portal(regions, 4, type[1], color[1]));
                arrowSprites.add(right);
                arrowSprites.add(down);
                break;
            case 7:
                portals.add(new Portal(regions, 1, type[0], color[0]));
                portals.add(new Portal(regions, 2, type[1], color[1]));
                portals.add(new Portal(regions, 4, type[2], color[2]));
                arrowSprites.add(up);
                arrowSprites.add(right);
                arrowSprites.add(down);
                break;
            case 8:
                portals.add(new Portal(regions, 8, type[0], color[0]));
                arrowSprites.add(left);
                break;
            case 9:
                portals.add(new Portal(regions, 1, type[0], color[0]));
                portals.add(new Portal(regions, 8, type[1], color[1]));
                arrowSprites.add(up);
                arrowSprites.add(left);
                break;
            case 10:
                portals.add(new Portal(regions, 2, type[0], color[0]));
                portals.add(new Portal(regions, 8, type[1], color[1]));
                arrowSprites.add(right);
                arrowSprites.add(left);
                break;
            case 11:
                portals.add(new Portal(regions, 1, type[0], color[0]));
                portals.add(new Portal(regions, 2, type[1], color[1]));
                portals.add(new Portal(regions, 8, type[2], color[2]));
                arrowSprites.add(up);
                arrowSprites.add(right);
                arrowSprites.add(left);
                break;
            case 12:
                portals.add(new Portal(regions, 4, type[0], color[0]));
                portals.add(new Portal(regions, 8, type[1], color[1]));
                arrowSprites.add(down);
                arrowSprites.add(left);
                break;
            case 13:
                portals.add(new Portal(regions, 1, type[0], color[0]));
                portals.add(new Portal(regions, 4, type[1], color[1]));
                portals.add(new Portal(regions, 8, type[2], color[2]));
                arrowSprites.add(up);
                arrowSprites.add(down);
                arrowSprites.add(left);
                break;
            case 14:
                portals.add(new Portal(regions, 2, type[0], color[0]));
                portals.add(new Portal(regions, 4, type[1], color[1]));
                portals.add(new Portal(regions, 8, type[2], color[2]));
                arrowSprites.add(right);
                arrowSprites.add(down);
                arrowSprites.add(left);
                break;
            case 15:
                portals.add(new Portal(regions, 1, type[0], color[0]));
                portals.add(new Portal(regions, 2, type[1], color[1]));
                portals.add(new Portal(regions, 4, type[2], color[2]));
                portals.add(new Portal(regions, 8, type[3], color[3]));
                arrowSprites.add(up);
                arrowSprites.add(right);
                arrowSprites.add(down);
                arrowSprites.add(left);
                break;
        }

        if (progress > Config.NINTH_LEVEL)
            progressDelay -= 200; // Lets' remove some of player's time to up the ante a little bit.

        progress++;
    }

    private void setupArrowSprites() {
        up = new Sprite(arrowTexture);
        right = new Sprite(arrowTexture);
        down = new Sprite(arrowTexture);
        left = new Sprite(arrowTexture);

        up.setPosition(Gdx.graphics.getWidth() / 2 - (up.getWidth() / 2), Gdx.graphics.getHeight() / 2 + up.getHeight());
        left.setPosition(Gdx.graphics.getWidth() / 2 - right.getWidth() * 2, Gdx.graphics.getHeight() / 2 - (right.getHeight() / 2));
        down.setPosition(Gdx.graphics.getWidth() / 2 - (down.getWidth() / 2), Gdx.graphics.getHeight() / 2 - (down.getHeight() * 2));
        right.setPosition(Gdx.graphics.getWidth() / 2 + (left.getWidth()), Gdx.graphics.getHeight() / 2 - (left.getHeight() / 2));

        up.setScale(.5f);
        down.setScale(.5f);
        right.setScale(.5f);
        left.setScale(.5f);

        up.setOrigin(up.getWidth() / 2, up.getHeight() / 2);
        left.setOrigin(right.getWidth() / 2, right.getHeight() / 2);
        down.setOrigin(down.getWidth() / 2, down.getHeight() / 2);
        right.setOrigin(left.getWidth() / 2, left.getHeight() / 2);

        left.rotate(90);
        down.rotate(180);
        right.rotate(270);
    }

    private void clearPortalsAndArrows() {
        for (Portal portal : portals) {
            portal.stopTweener();
        }

        portals.clear();
        arrowSprites.clear();
    }

    private void getColors(int progress) {
        if (progress < Config.FIRST_LEVEL) {
            color[0] = color[1] = color[2] = color[3] = colors[0]; // All are the same
        } else if (progress < Config.THIRD_LEVEL) {
            if (MathUtils.randomBoolean()) {
                color[0] = color[1] = colors[MathUtils.random(0, Config.NUM_COLORS - 1)]; // Two different colors
                color[2] = color[3] = colors[MathUtils.random(0, Config.NUM_COLORS - 1)];
            } else {
                color[0] = color[2] = colors[MathUtils.random(0, Config.NUM_COLORS - 1)]; // Two different colors
                color[1] = color[3] = colors[MathUtils.random(0, Config.NUM_COLORS - 1)];
            }
        } else if (progress < Config.FIFTH_LEVEL) { // Three different colors
            if (MathUtils.randomBoolean()) {
                color[0] = color[1] = colors[MathUtils.random(0, Config.NUM_COLORS - 1)];
                color[2] = colors[MathUtils.random(0, Config.NUM_COLORS - 1)];
                color[3] = colors[MathUtils.random(0, Config.NUM_COLORS - 1)];
            } else {
                color[0] = color[2] = colors[MathUtils.random(0, Config.NUM_COLORS - 1)];
                color[1] = colors[MathUtils.random(0, Config.NUM_COLORS - 1)];
                color[3] = colors[MathUtils.random(0, Config.NUM_COLORS - 1)];
            }
        } else { // All colors
            color[0] = colors[MathUtils.random(0, Config.NUM_COLORS - 1)];
            color[1] = colors[MathUtils.random(0, Config.NUM_COLORS - 1)];
            color[2] = colors[MathUtils.random(0, Config.NUM_COLORS - 1)];
            color[3] = colors[MathUtils.random(0, Config.NUM_COLORS - 1)];
        }
    }

    // Trying to make some interesting progression
    private void getTypes(int progress) {
        if (progress < Config.SECOND_LEVEL) {
            type[0] = type[1] = type[2] = type[3] = MathUtils.random(0, Config.NUM_TYPES - 1); // All are the same
        } else if (progress < Config.FOURTH_LEVEL) {
            if (MathUtils.randomBoolean()) {
                type[0] = type[1] = MathUtils.random(0, Config.NUM_TYPES - 1); // Two different types
                type[2] = type[3] = MathUtils.random(0, Config.NUM_TYPES - 1);
            } else {
                type[0] = type[2] = MathUtils.random(0, Config.NUM_TYPES - 1); // Two different types
                type[1] = type[3] = MathUtils.random(0, Config.NUM_TYPES - 1);
            }
        } else if (progress < Config.SIXTH_LEVEL) { // Three different types
            if (MathUtils.randomBoolean()) {
                type[0] = type[1] = MathUtils.random(0, Config.NUM_TYPES - 1);
                type[2] = MathUtils.random(0, Config.NUM_TYPES - 1);
                type[3] = MathUtils.random(0, Config.NUM_TYPES - 1);
            } else {
                type[0] = type[2] = MathUtils.random(0, Config.NUM_TYPES - 1);
                type[1] = MathUtils.random(0, Config.NUM_TYPES - 1);
                type[3] = MathUtils.random(0, Config.NUM_TYPES - 1);
            }
        } else { // Four different types
            type[0] = MathUtils.random(0, Config.NUM_TYPES - 1);
            type[1] = MathUtils.random(0, Config.NUM_TYPES - 1);
            type[2] = MathUtils.random(0, Config.NUM_TYPES - 1);
            type[3] = MathUtils.random(0, Config.NUM_TYPES - 1);
        }
    }
}
