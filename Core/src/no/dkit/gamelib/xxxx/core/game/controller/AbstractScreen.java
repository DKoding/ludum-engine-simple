package no.dkit.gamelib.xxxx.core.game.controller;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.bitfire.postprocessing.PostProcessor;
import com.bitfire.postprocessing.effects.Bloom;
import com.bitfire.postprocessing.effects.Curvature;
import com.bitfire.postprocessing.effects.Vignette;
import com.bitfire.utils.ShaderLoader;
import no.dkit.gamelib.xxxx.core.XXXX;
import no.dkit.gamelib.xxxx.core.game.Config;
import no.dkit.gamelib.xxxx.core.game.factory.EffectFactory;
import no.dkit.gamelib.xxxx.core.game.factory.SoundFactory;
import no.dkit.gamelib.xxxx.core.game.view.TweenCameraAccessor;
import no.dkit.gamelib.xxxx.core.game.view.table.ControllableTable;
import no.dkit.gamelib.xxxx.core.helpers.Measure;

import java.util.ArrayList;
import java.util.List;

import static no.dkit.gamelib.xxxx.core.game.Config.PAUSED;

public abstract class AbstractScreen implements Screen {
    Array<Timer.Task> tasks = new Array<>();

    InputMultiplexer multiplexer;
    GestureDetector detector;
    ActorControllerListener listener;
    Stage stage;
    SpriteBatch spriteBatch;
    List<Actor> controllableActors = new ArrayList<Actor>();
    OrthographicCamera camera;
    private long frameTime;
    private long fpsStart;
    Measure tweenerMeasure = new Measure("TWEENER UPDATE", 100);

    protected PostProcessor postProcessor;

    protected Bloom bloom;
    private Curvature curvature;
    protected Vignette vignette;

    public AbstractScreen() {
        Gdx.input.setCatchBackKey(true);
        Gdx.input.setCatchMenuKey(true);

        this.listener = new ActorControllerListener(this);
        this.detector = new GestureDetector(listener);

        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(false);

        spriteBatch = new SpriteBatch();
        spriteBatch.setProjectionMatrix(camera.combined);

        stage = new Stage(new FillViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), camera), spriteBatch);

        configurePostProcessor();
    }

    protected void configurePostProcessor() {
        ShaderLoader.BasePath = "shaders/";
        postProcessor = new PostProcessor(false, false, (Gdx.app.getType() == Application.ApplicationType.Desktop));

        curvature = new Curvature();
        bloom = new Bloom(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 4);
        vignette = new Vignette(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);

        postProcessor.addEffect(bloom);
        postProcessor.addEffect(curvature);
        postProcessor.addEffect(vignette);

        updatePostProcessors();
    }

    protected void updatePostProcessors() {
        bloom.setBaseIntesity(1f);
        bloom.setBloomIntesity(1.2f);
        vignette.setSaturation(2f);
        vignette.setSaturationMul(1.1f);
    }

    public ActorControllerListener getListener() {
        return listener;
    }

    public List<Actor> getControllableActors() {
        return controllableActors;
    }

    private void refreshInputListener(Stage stage) {
        this.multiplexer = new InputMultiplexer(stage, listener, detector); // Don't know why not using a detector above works
        Gdx.input.setInputProcessor(multiplexer);
    }

    public void refreshInputListener() {
        Controllers.clearListeners();
        Controllers.addListener(listener);
        refreshInputListener(stage);
    }

    @Override
    public void render(float delta) {
        heartBeat();

        if (XXXX.gameState != XXXX.GAME_STATE.RUNNING && XXXX.gameState != XXXX.GAME_STATE.CLICK_TO_CONTINUE) return;

        frameTime = 0;
        fpsStart = System.currentTimeMillis();

        if (listener != null)
            listener.update();

        updateTweener(delta);

        if (Gdx.input.isKeyJustPressed(Input.Keys.D))
            performDebugAction();

        /*
        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER))
            pauseOrUnpause();

        if (Gdx.input.isKeyJustPressed(Input.Keys.R))
            XXXX.changeScreen(XXXX.SCREEN.NEWLEVEL);

        if (Gdx.input.isKeyJustPressed(Input.Keys.N)) {
            XXXX.changeScreen(XXXX.SCREEN.NEWLEVEL);
        }
*/
        if (PAUSED) return;

        updateModel(delta);
        updateView(delta);

        frameTime = System.currentTimeMillis() - fpsStart;
    }

    private void heartBeat() {
        SoundFactory.getInstance().heartbeat();
    }

    protected abstract void updateModel(float delta);

    protected abstract void updateView(float delta);

    protected abstract void onStart();

    public void addControllableActor(ControllableTable table) {
        addControllableActor(table, true);
    }

    public void addControllableActor(ControllableTable table, boolean pause) {
        if (pause)
            XXXX.gameState = XXXX.GAME_STATE.PAUSING;

        stage.addActor(table);
        controllableActors = table.getControllableActors();
        refreshInputListener(stage);
    }


    public void removeControllableActor(ControllableTable table) {
        table.remove();
        controllableActors.clear();
        refreshInputListener(stage);
        XXXX.gameState = XXXX.GAME_STATE.RUNNING;
    }

    private boolean hasControllers() {
        System.out.println("Checking for controllers...");
        listener.hasControllers = Controllers.getControllers().size > 0;

        if (listener.hasControllers)
            System.out.println(Controllers.getControllers().size + " controllers found" + Controllers.getControllers().get(0).toString());

        return listener.hasControllers;
    }

    @Override
    public void show() {
        if (XXXX.gameState == XXXX.GAME_STATE.INIT) {
            XXXX.gameState = XXXX.GAME_STATE.STARTING;
            onStart();
        }

        XXXX.gameState = XXXX.GAME_STATE.RUNNING;

        if (Config.music)
            SoundFactory.getInstance().playMusic(SoundFactory.MUSIC_TYPE.GAME);

        refreshInputListener();
    }

    @Override
    public void hide() {
        XXXX.gameState = XXXX.GAME_STATE.HIDDEN;
        if (Config.music)
            SoundFactory.getInstance().stopMusic();
    }

    public void pause() {
        XXXX.gameState = XXXX.GAME_STATE.PAUSING;
        if (Config.music)
            SoundFactory.getInstance().stopMusic();
    }

    public void resume() {
        XXXX.gameState = XXXX.GAME_STATE.RESUMING;
        if (Config.music)
            SoundFactory.getInstance().resumeMusic();
        refreshInputListener();
    }

    public IntMap<Float> getInput(int controllerNum) {
        return listener.getInput(controllerNum);
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
        camera.viewportWidth = width;
        camera.viewportHeight = height;
        camera.update();
        spriteBatch.setProjectionMatrix(camera.combined);
    }

    @Override
    public void dispose() {
        stage.dispose();
        spriteBatch.dispose();
        postProcessor.dispose();
        cancelAllTasks();
    }

    public void cancelAllTasks() {
        for (Timer.Task task : tasks) {
            task.cancel();
        }

        tasks.clear();
    }

    public Stage getStage() {
        return stage;
    }

    private void updateTweener(float delta) {
        tweenerMeasure.start();
        XXXX.getTweener().update(delta);
        tweenerMeasure.end();
    }

    protected void performDebugAction() {
        cameraEffect();
        EffectFactory.getInstance().addEffect(MathUtils.random(Gdx.graphics.getWidth()), MathUtils.random(Gdx.graphics.getHeight()), EffectFactory.EFFECT_TYPE.ACHIEVE, EffectFactory.DRAW_LAYER.UI);
    }

    protected void cameraEffect() {
        Timeline.createSequence().beginParallel()
                .push(Tween.to(camera, TweenCameraAccessor.ZOOM, .05f).target(MathUtils.random(.9f, 1.1f)).repeatYoyo(5, 0))
                .push(Tween.to(camera, TweenCameraAccessor.ROTATION_DEG, .05f).target(MathUtils.random(-3, 3)).repeatYoyo(5, 0))
                .end().beginParallel()
                .push(Tween.to(camera, TweenCameraAccessor.ZOOM, .05f).target(1f))
                .push(Tween.to(camera, TweenCameraAccessor.ROTATION_DEG, .05f).target(0f))
                .end()
                .start(XXXX.getTweener());
    }

    protected void pauseOrUnpause() {
        PAUSED = !PAUSED;
    }
}
