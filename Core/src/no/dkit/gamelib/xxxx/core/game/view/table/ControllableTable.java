package no.dkit.gamelib.xxxx.core.game.view.table;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import no.dkit.gamelib.xxxx.core.XXXX;
import no.dkit.gamelib.xxxx.core.game.Config;
import no.dkit.gamelib.xxxx.core.game.controller.ComponentFactory;
import no.dkit.gamelib.xxxx.core.game.controller.ImageListener;
import no.dkit.gamelib.xxxx.core.game.factory.ResourceFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Remember to update the tweener during rendering if you use this
 */
public abstract class ControllableTable extends Table {     // TODO: Make a more basic table as the superclass, this is made for dialogs and loot
    public static final String TRANSPARENT = "transparent";
    boolean canBeClosed = false;
    protected Button closeButton;
    protected Button actionButton;
    protected Label titleLabel;
    protected String title;
    protected int screenWidth;
    protected int screenHeight;
    protected int tableWidth;
    protected int tableHeight;
    protected int tableXPos;
    protected int tableYPos;
    protected int padding;

    protected List<Actor> controllableActors = new ArrayList<Actor>();
    private Table buttonGroup;

    public ControllableTable(boolean canBeClosed) {
        this.canBeClosed = canBeClosed;
        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        setSkin(XXXX.skin);
        defaults().expand();
        setupDimensions();
        setupPosition();

        setupTitleUI();
        setupActionButtonGroup();

        add(titleLabel).width(tableWidth).center();
        row();
        add(getContentTable()).width(tableWidth).center();
        row();
        add(buttonGroup).align(canBeClosed ? Align.right | Align.bottom : Align.center | Align.bottom);

        setSize(tableWidth, tableHeight);
        setPosition(tableXPos, tableYPos);

        if (Config.DEBUG)
            setDebug(true);
    }

    protected abstract Table getContentTable();

    protected void setupPosition() {
        tableXPos = (screenWidth - tableWidth) / 2;
        tableYPos = (screenHeight - tableHeight) / 2;
    }

    protected void setupTitleUI() {
        titleLabel = new Label(title, XXXX.skin, "large");
        titleLabel.setFontScale(1f);
        titleLabel.setWrap(false);
        titleLabel.setAlignment(Align.top | Align.center);
        titleLabel.setWidth(tableWidth);
    }

    protected Table setupActionButtonGroup() {
        buttonGroup = new Table();

        if (this.canBeClosed) {
            closeButton = ComponentFactory.createTransparentButton(ResourceFactory.getInstance().getImage(ResourceFactory.UI, "close"), new ImageListener.DefaultListener() {
                @Override
                public void onClick() {
                    onCloseClick();
                }
            });
            buttonGroup.add(closeButton).size(Gdx.graphics.getWidth() / 20);
            controllableActors.add(closeButton);
        }

        actionButton = ComponentFactory.createTransparentButton(ResourceFactory.getInstance().getImage(ResourceFactory.UI, "start"), new ImageListener.DefaultListener() {
            @Override
            public void onClick() {
                onActionClick();
            }
        });

        buttonGroup.add(actionButton).size(Gdx.graphics.getWidth() / 20);
        controllableActors.add(actionButton);

        buttonGroup.pack();

        return buttonGroup;
    }

    protected void setupDimensions() {
        tableWidth = 600;
        tableHeight = 400;
    }

    protected abstract void onCloseClick();

    protected abstract void onActionClick();

    public List<Actor> getControllableActors() {
        return controllableActors;
    }
}
