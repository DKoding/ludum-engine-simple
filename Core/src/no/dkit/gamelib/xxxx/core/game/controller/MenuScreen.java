package no.dkit.gamelib.xxxx.core.game.controller;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.equations.Expo;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import no.dkit.gamelib.xxxx.core.XXXX;
import no.dkit.gamelib.xxxx.core.game.Config;
import no.dkit.gamelib.xxxx.core.game.factory.EffectFactory;
import no.dkit.gamelib.xxxx.core.game.factory.ResourceFactory;
import no.dkit.gamelib.xxxx.core.game.factory.SoundFactory;
import no.dkit.gamelib.xxxx.core.game.view.TweenActorAccessor;

public class MenuScreen extends AbstractScreen {
    private final Image shapeMatchActor;
    private final Image beatActor;
    private final Image jamActor;

    public MenuScreen() {
        TextureRegion playImage = ResourceFactory.getInstance().getImage(ResourceFactory.UI, "play");
        TextureRegion shapematch = ResourceFactory.getInstance().getImage(ResourceFactory.UI, "shapematch");
        TextureRegion beat = ResourceFactory.getInstance().getImage(ResourceFactory.UI, "beat");
        TextureRegion jam = ResourceFactory.getInstance().getImage(ResourceFactory.UI, "jam");

        final Image image = new Image(playImage);
        Button button = new Button(image, XXXX.skin, ComponentFactory.TRANSPARENT);
        button.addListener(new ImageListener(image, new ImageListener.DefaultListener() {
            @Override
            public void onClick() {
                XXXX.numLevels = 0;
                XXXX.changeScreen(XXXX.SCREEN.GAME);
            }   // WAS: Overview planets
        }));

        button.setOrigin(Align.center);
        button.setPosition(Gdx.graphics.getWidth() / 2 - button.getWidth() / 2, Gdx.graphics.getHeight() /4);

        shapeMatchActor = createLogoImage(shapematch, 0);
        beatActor = createLogoImage(beat, 1);
        jamActor = createLogoImage(jam, 2);

        final Label label = new Label("HIGHSCORE: " + XXXX.HIGHSCORE, XXXX.skin, "small");
        label.setColor(Color.GOLDENROD);
        label.setAlignment(Align.center);
        label.setPosition(Gdx.graphics.getWidth() / 2 - label.getWidth() / 2, Gdx.graphics.getHeight() / 2 - label.getHeight());

        final Label byline1 = new Label("Made by KodingNights", XXXX.skin, "small");
        byline1.setColor(Color.GOLDENROD);
        byline1.setAlignment(Align.center);
        byline1.setPosition(Gdx.graphics.getWidth() / 2 - byline1.getWidth() / 2, byline1.getHeight()*3);

        final Label byline2 = new Label("Ludum Dare 35 - Shape Shifting", XXXX.skin, "small");
        byline2.setColor(Color.GOLDENROD);
        byline2.setAlignment(Align.center);
        byline2.setPosition(Gdx.graphics.getWidth() / 2 - byline2.getWidth() / 2, byline2.getHeight());

        stage.addActor(shapeMatchActor);
        stage.addActor(beatActor);
        stage.addActor(jamActor);
        stage.addActor(label);
        stage.addActor(byline1);
        stage.addActor(byline2);

        stage.addActor(button);
//        stage.addActor(settingsButton);

        controllableActors.add(button);

        tasks.add(Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                shapeMatchEffect(shapeMatchActor);

                tasks.add(Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        jamEffect(jamActor);
                        tasks.add(Timer.schedule(new Timer.Task() {
                            @Override
                            public void run() {
                                beatEffect(beatActor);

                                tasks.add(Timer.schedule(new Timer.Task() {
                                    boolean bigstart = true;

                                    @Override
                                    public void run() {
                                        if (bigstart) {
                                            cameraEffect();
                                            for (int i = 0; i < 20; i++)
                                                EffectFactory.getInstance().addEffect(MathUtils.random(Gdx.graphics.getWidth()), MathUtils.random(Gdx.graphics.getHeight()), EffectFactory.EFFECT_TYPE.ACHIEVE, EffectFactory.DRAW_LAYER.UI);
                                            bigstart = false;
                                        } else {
                                            EffectFactory.getInstance().addEffect(MathUtils.random(Gdx.graphics.getWidth()), MathUtils.random(Gdx.graphics.getHeight()), EffectFactory.EFFECT_TYPE.ACHIEVE, EffectFactory.DRAW_LAYER.UI);
                                            cameraEffect();
                                        }
                                    }
                                }, Config.TIME_UNTIL_NEXT_BEAT / 1000f, 1f));
                            }
                        }, 7f));
                    }
                }, 7f));
            }
        }, 2f));
    }

    private Image createLogoImage(TextureRegion logo, int num) {
        Image imageActor = new Image(logo);
        imageActor.setSize(logo.getRegionWidth() > Gdx.graphics.getWidth() ? Gdx.graphics.getWidth() : logo.getRegionWidth(),
                logo.getRegionHeight() > Gdx.graphics.getHeight() / 4 ? Gdx.graphics.getHeight() / 4 : logo.getRegionHeight());
        imageActor.setOrigin(Align.center);
        if (num == 0)
            imageActor.setPosition((Gdx.graphics.getWidth() - imageActor.getWidth()) / 2, Gdx.graphics.getHeight() - imageActor.getHeight() * 2);
        else if (num == 1)
            imageActor.setPosition((Gdx.graphics.getWidth() - imageActor.getWidth()) / 2, Gdx.graphics.getHeight() - imageActor.getHeight() * 3);
        else
            imageActor.setPosition((Gdx.graphics.getWidth() - imageActor.getWidth()) / 2, Gdx.graphics.getHeight() - imageActor.getHeight() * 4);

        return imageActor;
    }

    private void drawBackground() {
        Gdx.graphics.getGL20().glClearColor(.5f, .5f, .5f, 1);
        Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    }

    public void shapeMatchEffect(Actor actor) {
        Tween.to(actor, TweenActorAccessor.SCALE_XY, 1f / 2f).target(.9f, .9f).ease(Expo.INOUT).repeatYoyo(-1, 1f / 2f)
                .delay(1f - (Config.TIME_UNTIL_NEXT_BEAT) / 1000f).start(XXXX.getTweener());
    }

    public void beatEffect(Actor actor) {
        Tween.to(actor, TweenActorAccessor.SCALE_XY, 1f / 16f).target(1.2f, 1.2f).ease(Expo.INOUT).repeatYoyo(-1, 1f / 16f)
                .delay(1f - (Config.TIME_UNTIL_NEXT_BEAT / 1000f)).start(XXXX.getTweener());
    }

    public void jamEffect(Actor actor) {
        Tween.to(actor, TweenActorAccessor.SCALE_XY, 1f / 4f).target(.9f, .9f).ease(Expo.INOUT).repeatYoyo(-1, 1f / 4f)
                .delay(1f - (Config.TIME_UNTIL_NEXT_BEAT) / 1000f).start(XXXX.getTweener());
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    @Override
    protected void updateModel(float delta) {
        stage.act();
        EffectFactory.getInstance().update(delta);
    }

    @Override
    protected void updateView(float delta) {
        updatePostProcessors();

        postProcessor.capture();
        drawBackground();
        stage.draw();
        spriteBatch.begin();
        EffectFactory.getInstance().drawEffects(spriteBatch, EffectFactory.DRAW_LAYER.UI);
        spriteBatch.end();
        postProcessor.render();
    }

    @Override
    protected void onStart() {
    }

    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
    }

    public void hide() {
        if (Config.music)
            SoundFactory.getInstance().stopMusic();
    }

    public void pause() {
        if (Config.music)
            SoundFactory.getInstance().stopMusic();
    }

    public void resume() {
        if (Config.music)
            SoundFactory.getInstance().resumeMusic();
        if (postProcessor != null)
            postProcessor.rebind();
    }
}
