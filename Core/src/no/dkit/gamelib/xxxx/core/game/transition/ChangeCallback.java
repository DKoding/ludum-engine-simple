package no.dkit.gamelib.xxxx.core.game.transition;

import com.badlogic.gdx.Screen;

public interface ChangeCallback {
    void onSwitch(Screen oldScreen, Screen newScreen);
}
