package no.dkit.gamelib.xxxx.core.game;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.IntMap;
import no.dkit.gamelib.xxxx.core.XXXX;
import no.dkit.gamelib.xxxx.core.game.controller.ActorControllerListener;
import no.dkit.gamelib.xxxx.core.game.factory.EffectFactory;

public class Player extends GameSprite {
    int currentColor = 0;
    Color[] colors;
    private final ParticleEffect particleEffect;
    private long lastTab = System.currentTimeMillis();

    public Player(TextureRegion[] regions, Color[] colors) {
        super(regions, 0);
        this.colors = colors;
        particleEffect = EffectFactory.getInstance().addDecalEffect((int) (getX() + (getWidth() / 2f)), (int) (getY() + (getHeight() / 2)), EffectFactory.EFFECT_TYPE.TRACK);
        particleEffect.getEmitters().get(0).setContinuous(true);
    }

    public void parseControllerInput(IntMap<Float> input) {
        if (XXXX.gameState == XXXX.GAME_STATE.CLICK_TO_CONTINUE) {
            for (IntMap.Entry<Float> item : input.entries()) {
                if (clickToContinue(item.value, item)) {
                    XXXX.changeScreen(XXXX.SCREEN.NEWLEVEL);
                }
            }
        } else if (XXXX.gameState == XXXX.GAME_STATE.RUNNING) {
            Float val;

            for (IntMap.Entry<Float> item : input.entries()) {
                val = item.value;

                if (val != 0) {
                    handleKey(val, item);
                }
            }
        }
    }

    protected boolean handleKey(Float val, IntMap.Entry<Float> item) {
        if (item.key == Input.Keys.TAB) {
            if (System.currentTimeMillis() < lastTab + 100) return true;

            lastTab = System.currentTimeMillis();
            currentColor++;

            if (currentColor > colors.length - 1)
                currentColor = 0;

            color.set(colors[currentColor]);
        }

        if (item.key == Input.Keys.Q) {
            currentColor = 0;
            color.set(colors[currentColor]);
        }

        if (item.key == Input.Keys.W) {
            currentColor = 1;
            color.set(colors[currentColor]);
        }

        if (item.key == Input.Keys.E) {
            currentColor = 2;
            color.set(colors[currentColor]);
        }

        if (item.key == Input.Keys.R) {
            currentColor = 3;
            color.set(colors[currentColor]);
        }

        if (item.key == Input.Keys.T) {
            currentColor = 4;
            color.set(colors[currentColor]);
        }

        if (item.key == Input.Keys.NUM_1 || item.key == Input.Keys.NUMPAD_1) {
            updateRegion(0);
        }

        if (item.key == Input.Keys.NUM_2 || item.key == Input.Keys.NUMPAD_2) {
            updateRegion(1);
        }

        if (item.key == Input.Keys.NUM_3 || item.key == Input.Keys.NUMPAD_3) {
            updateRegion(2);
        }

        if (item.key == Input.Keys.NUM_4 || item.key == Input.Keys.NUMPAD_4) {
            updateRegion(3);
        }

        if (item.key == Input.Keys.NUM_5 || item.key == Input.Keys.NUMPAD_5) {
            updateRegion(4);
        }

        if (item.key == Input.Keys.UP) {
            if (!active) return false;
            tweenTo(up);
        }

        if (item.key == Input.Keys.DOWN) {
            if (!active) return false;
            tweenTo(down);
        }

        if (item.key == Input.Keys.RIGHT) {
            if (!active) return false;
            tweenTo(right);
        }

        if (item.key == Input.Keys.LEFT) {
            if (!active) return false;
            tweenTo(left);
        }

        if (item.key == Input.Keys.SPACE) {
            if (!active) return false;
            tweenTo(center);
        }

        return true;
    }

    protected boolean handleButton(Float val, IntMap.Entry<Float> item) {
        if (item.key == ActorControllerListener.BUTTON_R && val > 0) {
            if (currentRegion < Config.NUM_TYPES) currentRegion++;
            updateRegion(currentRegion);
        } else if (item.key == ActorControllerListener.BUTTON_L && val > 0) {
            if (currentRegion > 0) currentRegion--;
            updateRegion(currentRegion);
        }

        return false;
    }

    protected boolean clickToContinue(Float val, IntMap.Entry<Float> item) {
        if (item.key == Input.Keys.SPACE && val > 0) {
            return true;
        }

        return false;
    }

    @Override
    public void draw(Batch batch) {
        particleEffect.setPosition((int) (getX() + (getWidth() / 2f)), (int) (getY() + (getHeight() / 2)));
        super.draw(batch);
    }

    public void dispose() {
        tweening = false;
        active = false;
        creating = false;
        if (pulseTween != null)
            pulseTween.kill();
        particleEffect.allowCompletion();
    }
}
