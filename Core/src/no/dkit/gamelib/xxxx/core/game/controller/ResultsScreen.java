package no.dkit.gamelib.xxxx.core.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import no.dkit.gamelib.xxxx.core.game.factory.EffectFactory;
import no.dkit.gamelib.xxxx.core.game.view.table.ResultsTable;

public class ResultsScreen implements Screen {
    Stage stage;
    private final OrthographicCamera camera;

    private long lastShaderUpdate = System.currentTimeMillis();
    private long shaderFrameTime = 1;
    private final ResultsTable helpTable;
    SpriteBatch spriteBatch;

    public ResultsScreen() {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(false);

        spriteBatch = new SpriteBatch();
        spriteBatch.setProjectionMatrix(camera.combined);

        stage = new Stage(new ScreenViewport(camera));
        helpTable = new ResultsTable();
        stage.addActor(helpTable);
    }

    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {

    }

    public void render(float delta) {
        if (MathUtils.random(50) == 1)
            EffectFactory.getInstance().addUIEffect(MathUtils.random(Gdx.graphics.getWidth()), MathUtils.random(Gdx.graphics.getHeight()),
                    EffectFactory.EFFECT_TYPE.ACHIEVE);
        Gdx.graphics.getGL20().glClearColor(0, 0, 0, 1);
        Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        EffectFactory.getInstance().update(Gdx.graphics.getDeltaTime());
        spriteBatch.begin();
        EffectFactory.getInstance().drawEffects(spriteBatch, EffectFactory.DRAW_LAYER.UI);
        spriteBatch.end();

        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }
}
