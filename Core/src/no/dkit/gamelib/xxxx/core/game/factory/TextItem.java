package no.dkit.gamelib.xxxx.core.game.factory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;

public class TextItem {
    private long startTime;
    private String text;
    private float screenX;
    private float screenY;
    private Color color;
    private float alpha;
    private Vector3 projection = new Vector3();

    public TextItem() {
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public float getX() {
        return screenX;
    }

    public float getY() {
        return screenY;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public void setPosition(float x, float y) {
        projection.set(x, y, 0);
        this.screenX = projection.x;
        this.screenY = projection.y;
    }

    public void updatePosition(float x, float y) {
        this.screenX = x;
        this.screenY = y;
    }

    public void setPosition(int x, int y) {
        this.screenX = x;
        this.screenY = y;
    }
}
