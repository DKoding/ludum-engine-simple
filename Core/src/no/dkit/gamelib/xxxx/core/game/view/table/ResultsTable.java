package no.dkit.gamelib.xxxx.core.game.view.table;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import no.dkit.gamelib.xxxx.core.XXXX;

public class ResultsTable extends ControllableTable {
    public ResultsTable() {
        super(false);
        titleLabel.setText("RESULTS AFTER " + XXXX.settings.getNumRounds() + " ROUNDS");
        XXXX.loadSettings();
    }

    @Override
    protected Table getContentTable() {
        Table table = new Table(XXXX.skin);
        table.pad(25);
        int rowWidth = tableWidth - 50;

        table.add("Rank", "large").width(rowWidth / 7).align(Align.left);
        table.add("Player", "large").width(rowWidth / 7).align(Align.left);
        table.add("Wins", "large").width(rowWidth / 7).align(Align.left);
        table.add("Kills", "large").width(rowWidth / 7).align(Align.left);
        table.add("Deaths", "large").width(rowWidth / 7).align(Align.left);
        table.add("Suicides", "large").width(rowWidth / 7).align(Align.left);
        table.add("Score", "large").width(rowWidth / 7).align(Align.left);
        table.row();

        return table;
    }

    @Override
    protected void onCloseClick() {
        XXXX.changeScreen(XXXX.SCREEN.STARTMENU);
    }

    @Override
    protected void onActionClick() {
        XXXX.changeScreen(XXXX.SCREEN.STARTMENU);
    }
}
