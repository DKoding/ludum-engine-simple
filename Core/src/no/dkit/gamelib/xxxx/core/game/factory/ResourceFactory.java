package no.dkit.gamelib.xxxx.core.game.factory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.StringBuilder;
import no.dkit.gamelib.xxxx.core.game.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ResourceFactory implements AssetErrorListener {
    public static final String UI = "ui";
    public static final String PARTICLE = "particle";

    static ResourceFactory resourceFactory;

    com.badlogic.gdx.utils.StringBuilder builder = new StringBuilder();

    String baseDir = "images/";

    AssetManager manager;

    int findRegionCalls = 0;
    int getTextureCalls = 0;
    int atlasCalls = 0;

    private TextureRegion[] regions;

    public static ResourceFactory getInstance() {
        if (resourceFactory == null)
            resourceFactory = new ResourceFactory();

        return resourceFactory;
    }

    protected ResourceFactory() {
        MathUtils.random.setSeed(666);

        manager = new AssetManager();
        manager.setErrorListener(this);

        List<String> atlases = new ArrayList<String>();
        atlases.addAll(Arrays.asList(UI, PARTICLE));

        for (String fileName : atlases) {
            manager.load(baseDir + fileName + ".atlas", TextureAtlas.class);
        }
    }

    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {
        Gdx.app.error("AssetManagerTest", "Couldn't load asset '" + asset.fileName + "'", throwable);
        throw new RuntimeException("Couldn't load asset '" + asset.fileName + "'", throwable);
    }

    public TextureAtlas getAtlas(String type) {
        atlasCalls++;
        builder.setLength(0);
        builder.append(baseDir).append(type).append(".atlas");
        return manager.get(builder.toString(), TextureAtlas.class);
    }

    // Return image based on level world type
    public TextureAtlas.AtlasRegion getPlayerTypeImage(int num) {
        return getImage(UI, "" + num);
    }

    public TextureAtlas.AtlasRegion getPortalTypeImage(int num) {
        return getImage(UI, "" + num);
    }

    public TextureAtlas.AtlasRegion getImage(String type, String imageName) {
        findRegionCalls++;

        TextureAtlas.AtlasRegion region = getAtlas(type).findRegion(imageName);

        if (region == null && !"".equals(imageName))
            throw new RuntimeException("Could not load image " + imageName + " for type " + type);

        return region;
    }

    public void dispose() {
        System.out.println("findRegionCalls = " + findRegionCalls);
        System.out.println("getTextureCalls = " + getTextureCalls);
        System.out.println("atlasCalls = " + atlasCalls);
        manager.dispose();
    }

    public boolean poll() {
        return manager.update();
    }

    public float getPercentComplete() {
        return manager.getProgress();
    }

    public TextureAtlas getParticleAtlas() {
        return getAtlas(PARTICLE);
    }

    public TextureRegion[] getRegions() {
        if (regions == null) {
            regions = new TextureRegion[Config.NUM_TYPES];

            for (int i = 0; i < regions.length; i++) {
                regions[i] = getImage(UI, "" + (i+1));
            }
        }

        return regions;
    }
}
