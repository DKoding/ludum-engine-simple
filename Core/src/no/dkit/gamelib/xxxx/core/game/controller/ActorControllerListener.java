package no.dkit.gamelib.xxxx.core.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.Pools;
import no.dkit.gamelib.xxxx.core.XXXX;
import no.dkit.gamelib.xxxx.core.game.Config;
import no.dkit.gamelib.xxxx.core.game.view.XBox360Pad;
import no.dkit.gamelib.xxxx.core.game.view.table.ConfirmTable;

public class ActorControllerListener implements ControllerListener, InputProcessor, GestureDetector.GestureListener {
    protected boolean hasControllers;
    ControllerInput[] controllerInput;

    public static final int LEFT_STICK_UP = -1;
    public static final int LEFT_STICK_DOWN = -2;
    public static final int LEFT_STICK_RIGHT = -3;
    public static final int LEFT_STICK_LEFT = -4;
    public static final int RIGHT_STICK_UP = -5;
    public static final int RIGHT_STICK_DOWN = -6;
    public static final int RIGHT_STICK_RIGHT = -7;
    public static final int RIGHT_STICK_LEFT = -8;
    public static final int PAD_N = -9;
    public static final int PAD_NE = -10;
    public static final int PAD_E = -11;
    public static final int PAD_SE = -12;
    public static final int PAD_S = -13;
    public static final int PAD_SW = -14;
    public static final int PAD_W = -15;
    public static final int PAD_NW = -16;
    public static final int TRIGGER_LEFT = -17;
    public static final int TRIGGER_RIGHT = -18;
    public static final int BUTTON_X = -19;
    public static final int BUTTON_Y = -20;
    public static final int BUTTON_A = -21;
    public static final int BUTTON_B = -22;
    public static final int BUTTON_R = -23;
    public static final int BUTTON_L = -24;
    public static final int BUTTON_START = -25;
    public static final int BUTTON_BACK = -26;
    public static final int MOUSE_LEFT = -27;
    public static final int MOUSE_RIGHT = -28;
    public static final int MOUSE_WHEEL_UP = -29;
    public static final int MOUSE_WHEEL_DOWN = -30;

    protected IntMap<Float>[] input;

    protected AbstractScreen screen;
    protected int currentButtonIndex = 0;

    public ActorControllerListener(AbstractScreen screen) {
        this.screen = screen;

        input = new IntMap[Config.MAX_NUMBER_OF_PLAYERS];

        for (int i = 0; i < Config.MAX_NUMBER_OF_PLAYERS; i++)
            input[i] = new IntMap<>();

        updateControllerInput();
    }

    public void update() {
        if (controllerInput == null) return;

        for (ControllerInput input : controllerInput) {
            if (input.num > Config.MAX_NUMBER_OF_PLAYERS) continue;
            processLeftStick(input);
            processRightStick(input);
            processDpad(input);
            processButtons(input);
            processTriggers(input);
        }
    }

    @Override
    public void connected(Controller controller) {
        hasControllers = Controllers.getControllers().size != 0;
        updateControllerInput();
        screen.refreshInputListener();
    }

    @Override
    public void disconnected(Controller controller) {
        screen.addControllableActor(new ConfirmTable("Controller disconnected", "Reattach controller to continue") {
            @Override
            protected void onActionClick() {
                onCloseClick();
            }
        });

        updateControllerInput();

        hasControllers = Controllers.getControllers().size == 0;
    }

    private void updateControllerInput() {
        final Array<Controller> controllers = Controllers.getControllers();
        controllerInput = new ControllerInput[controllers.size];

        for (int i = 0; i < controllers.size; i++)
            controllerInput[i] = new ControllerInput(i, controllers.get(i));
    }

    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        if (screen.getControllableActors().size() == 0) return buttonDownNoActor(controller, buttonCode);
        switch (buttonCode) {
            case XBox360Pad.BUTTON_A:
                Actor currentButton = screen.getControllableActors().get(currentButtonIndex);
                return touchButton(currentButton);
        }

        return povMoved(controller, buttonCode);
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        if (screen.getControllableActors().size() == 0) return buttonUpNoActor(controller, buttonCode);
        switch (buttonCode) {
            case XBox360Pad.BUTTON_A:
                Actor currentButton = screen.getControllableActors().get(currentButtonIndex);
                return releaseButton(currentButton);
        }
        return false;
    }

    @Override
    public boolean povMoved(Controller controller, int povCode, PovDirection value) {
        return false;
    }

    private boolean povMoved(Controller controller, int value) {
        if (screen.getControllableActors().size() == 0) return povMovedNoActor(controller, value);
        unselectButton(screen.getControllableActors().get(currentButtonIndex));

        switch (value) {
            case XBox360Pad.BUTTON_POV_UP:
            case XBox360Pad.BUTTON_POV_LEFT:
                currentButtonIndex--;
                break;
            case XBox360Pad.BUTTON_POV_DOWN:
            case XBox360Pad.BUTTON_POV_RIGHT:
                currentButtonIndex++;
                break;
            default:
                break;
        }

        currentButtonIndex = currentButtonIndex % screen.getControllableActors().size();
        if (currentButtonIndex < 0) currentButtonIndex = screen.getControllableActors().size() - 1;

        System.out.println("currentButtonIndex = " + currentButtonIndex);

        return selectButton(screen.getControllableActors().get(currentButtonIndex));
    }

    private boolean buttonDownNoActor(Controller controller, int buttonCode) {
        for (ControllerInput input : controllerInput) {
            if (input.controller == controller) {
                if (buttonCode == XBox360Pad.BUTTON_Y) {
                    input.buttonY = true;
                }
                if (buttonCode == XBox360Pad.BUTTON_A) {
                    input.buttonA = true;
                }
                if (buttonCode == XBox360Pad.BUTTON_X) {
                    input.buttonX = true;
                }
                if (buttonCode == XBox360Pad.BUTTON_B) {
                    input.buttonB = true;
                }
                if (buttonCode == XBox360Pad.BUTTON_LB) {
                    input.buttonL1 = true;
                }
                if (buttonCode == XBox360Pad.BUTTON_RB) {
                    input.buttonR1 = true;
                }
            }
        }
        return false;
    }

    private boolean buttonUpNoActor(Controller controller, int buttonCode) {
        for (ControllerInput input : controllerInput) {
            if (input.controller == controller) {
                if (buttonCode == XBox360Pad.BUTTON_Y) {
                    input.buttonY = false;
                }
                if (buttonCode == XBox360Pad.BUTTON_A) {
                    input.buttonA = false;
                }
                if (buttonCode == XBox360Pad.BUTTON_X) {
                    input.buttonX = false;
                }
                if (buttonCode == XBox360Pad.BUTTON_B) {
                    input.buttonB = false;
                }
                if (buttonCode == XBox360Pad.BUTTON_LB) {
                    input.buttonL1 = false;
                }
                if (buttonCode == XBox360Pad.BUTTON_RB) {
                    input.buttonR1 = false;
                }
            }
        }
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int axisCode, float value) {
        System.out.printf("AXIS %s VALUE %s", axisCode, value);

        for (ControllerInput input : controllerInput) {
            if (input.controller == controller) {

                // Left Stick
                if (axisCode == XBox360Pad.AXIS_LEFT_X) {
                    input.leftAxisX = value;
                }

                if (axisCode == XBox360Pad.AXIS_LEFT_Y) {
                    input.leftAxisY = value;
                }

                // Right stick
                if (axisCode == XBox360Pad.AXIS_RIGHT_X) {
                    input.rightAxisX = value;
                }
                if (axisCode == XBox360Pad.AXIS_RIGHT_Y) {
                    input.rightAxisY = value;
                }

                //triggerLeft = triggerRight = false;

                if (axisCode == XBox360Pad.AXIS_LEFT_TRIGGER) {  // LEFT and RIGHT trigger is the same axis
                    input.triggerLeft = value > .8f;                // TODO: Calibrate?
                    input.triggerRight = value < -.8f;
                }

            }
        }
        return false;
    }

    private boolean povMovedNoActor(Controller controller, int value) {
        for (ControllerInput input : controllerInput) {
            if (input.controller == controller)
                switch (value) {
                    case XBox360Pad.BUTTON_POV_DOWN:
                        input.buttonPovS = true;
                        break;
                    case XBox360Pad.BUTTON_POV_RIGHT:
                        input.buttonPovE = true;
                        break;
                    case XBox360Pad.BUTTON_POV_UP:
                        input.buttonPovN = true;
                        break;
                    case XBox360Pad.BUTTON_POV_LEFT:
                        input.buttonPovW = true;
                        break;
                }

        }
        return false;
    }

    @Override
    public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
        return false;
    }

    private void processTriggers(ControllerInput input) {
        if (input.triggerLeft)
            addKeyPress(TRIGGER_LEFT, input.num);
        else
            removeKeyPress(TRIGGER_LEFT, input.num);

        if (input.triggerRight)
            addKeyPress(TRIGGER_RIGHT, input.num);
        else
            removeKeyPress(TRIGGER_RIGHT, input.num);
    }

    private void processButtons(ControllerInput input) {
        if (input.buttonY)
            addKeyPress(BUTTON_Y, input.num);
        else
            removeKeyPress(BUTTON_Y, input.num);

        if (input.buttonA)
            addKeyPress(BUTTON_A, input.num);
        else
            removeKeyPress(BUTTON_A, input.num);

        if (input.buttonX)
            addKeyPress(BUTTON_X, input.num);
        else
            removeKeyPress(BUTTON_X, input.num);

        if (input.buttonB)
            addKeyPress(BUTTON_B, input.num);
        else
            removeKeyPress(BUTTON_B, input.num);

        if (input.buttonL1)
            addKeyPress(BUTTON_L, input.num);
        else
            removeKeyPress(BUTTON_L, input.num);

        if (input.buttonR1)
            addKeyPress(BUTTON_R, input.num);
        else
            removeKeyPress(BUTTON_R, input.num);
    }

    private void processDpad(ControllerInput input) {
        if (input.buttonPovN)
            addKeyPress(PAD_N, input.num);
        else
            removeKeyPress(PAD_N, input.num);

        if (input.buttonPovN && input.buttonPovE)
            addKeyPress(PAD_NE, input.num);
        else
            removeKeyPress(PAD_NE, input.num);

        if (input.buttonPovE)
            addKeyPress(PAD_E, input.num);
        else
            removeKeyPress(PAD_E, input.num);

        if (input.buttonPovS && input.buttonPovE)
            addKeyPress(PAD_SE, input.num);
        else
            removeKeyPress(PAD_SE, input.num);

        if (input.buttonPovS)
            addKeyPress(PAD_S, input.num);
        else
            removeKeyPress(PAD_S, input.num);

        if (input.buttonPovS && input.buttonPovW)
            addKeyPress(PAD_SW, input.num);
        else
            removeKeyPress(PAD_SW, input.num);

        if (input.buttonPovW)
            addKeyPress(PAD_W, input.num);
        else
            removeKeyPress(PAD_W, input.num);

        if (input.buttonPovN && input.buttonPovW)
            addKeyPress(PAD_NW, input.num);
        else
            removeKeyPress(PAD_NW, input.num);
    }

    private void processLeftStick(ControllerInput input) {
        if (input.leftAxisX < -Config.CONTROLLER_DEADZONE)
            addKeyPress(LEFT_STICK_LEFT, -input.leftAxisX, input.num);
        else
            removeKeyPress(LEFT_STICK_LEFT, input.num);

        if (input.leftAxisX > Config.CONTROLLER_DEADZONE)
            addKeyPress(LEFT_STICK_RIGHT, input.leftAxisX, input.num);
        else
            removeKeyPress(LEFT_STICK_RIGHT, input.num);

        if (input.leftAxisY < -Config.CONTROLLER_DEADZONE)
            addKeyPress(LEFT_STICK_UP, -input.leftAxisY, input.num);
        else
            removeKeyPress(LEFT_STICK_UP, input.num);

        if (input.leftAxisY > Config.CONTROLLER_DEADZONE)
            addKeyPress(LEFT_STICK_DOWN, input.leftAxisY, input.num);
        else
            removeKeyPress(LEFT_STICK_DOWN, input.num);
    }

    private void processRightStick(ControllerInput input) {
        if (input.rightAxisX < -Config.CONTROLLER_DEADZONE)
            addKeyPress(RIGHT_STICK_LEFT, -input.rightAxisX, input.num);
        else
            removeKeyPress(RIGHT_STICK_LEFT, input.num);

        if (input.rightAxisX > Config.CONTROLLER_DEADZONE)
            addKeyPress(RIGHT_STICK_RIGHT, input.rightAxisX, input.num);
        else
            removeKeyPress(RIGHT_STICK_RIGHT, input.num);

        if (input.rightAxisY < -Config.CONTROLLER_DEADZONE)
            addKeyPress(RIGHT_STICK_UP, -input.rightAxisY, input.num);
        else
            removeKeyPress(RIGHT_STICK_UP, input.num);

        if (input.rightAxisY > Config.CONTROLLER_DEADZONE)
            addKeyPress(RIGHT_STICK_DOWN, input.rightAxisY, input.num);
        else
            removeKeyPress(RIGHT_STICK_DOWN, input.num);
    }

    private void addKeyPress(int i, int controllerNum) {
        input[controllerNum].put(i, 1f);
    }

    private void addKeyPress(int i, float value, int controllerNum) {
        input[controllerNum].put(i, value);
    }

    private void removeKeyPress(int i, int controllerNum) {
        //System.out.println("Removing keypress i = " + i);
        input[controllerNum].remove(i);
    }

    // Requires clickable component
    protected boolean touchButton(Actor button) {
        return fireEventOn(button, InputEvent.Type.touchDown);
    }

    protected boolean releaseButton(Actor button) {
        return fireEventOn(button, InputEvent.Type.touchUp);
    }

    protected boolean selectButton(Actor button) {
        return fireEventOn(button, InputEvent.Type.enter);
    }

    protected boolean unselectButton(Actor button) {
        return fireEventOn(button, InputEvent.Type.exit);
    }

    private boolean fireEventOn(Actor button, InputEvent.Type eventType) {
        InputEvent event = Pools.obtain(InputEvent.class);
        event.setType(eventType);
        event.setButton(Input.Buttons.LEFT);

        button.fire(event);
        boolean handled = event.isHandled();
        Pools.free(event);

        return handled;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (screen.getControllableActors().size() == 0) {
            removeKeyPress(keycode, 0);
            return true;
        }
        return keyUpActor(keycode);
    }

    @Override
    public boolean keyDown(int keycode) {
        if (specialKey(keycode)) return true;

        if (screen.getControllableActors().size() == 0) {
            addKeyPress(keycode, 0);
            return true;
        }
        return keyDownActor(keycode);
    }

    private boolean specialKey(int keycode) {
        if (keycode == Input.Keys.ESCAPE) {
            onQuit();
            return true;
        }

        if ((keycode == Input.Keys.BACKSPACE || keycode == Input.Keys.BACK)) {
            onBack();
            return true;
        }

        return false;
    }

    private void onBack() {
        if (!(XXXX.getGame().getScreen() instanceof MenuScreen))
            XXXX.changeScreen(XXXX.SCREEN.STARTMENU);
    }

    private void onQuit() {
        XXXX.gameState = XXXX.GAME_STATE.PAUSING;
        XXXX.getGame().getScreen().dispose();
        Gdx.app.exit();
    }

    private boolean keyDownActor(int keycode) {
        boolean clickedActor = keyClickActor(keycode);
        if (clickedActor) return true;
        unselectButton(screen.getControllableActors().get(currentButtonIndex));
        return keyDownChangeActor(keycode);
    }

    private boolean keyDownChangeActor(int keycode) {
        switch (keycode) {
            case Input.Keys.DOWN:
            case Input.Keys.RIGHT:
                currentButtonIndex++;
                break;
            case Input.Keys.UP:
            case Input.Keys.LEFT:
                currentButtonIndex--;
                break;
        }

        currentButtonIndex = currentButtonIndex % screen.getControllableActors().size();
        if (currentButtonIndex < 0) currentButtonIndex = screen.getControllableActors().size() - 1;

        System.out.println("currentButtonIndex = " + currentButtonIndex);

        return selectButton(screen.getControllableActors().get(currentButtonIndex));
    }

    private boolean keyClickActor(int keycode) {
        switch (keycode) {
            case Input.Keys.ENTER:
            case Input.Keys.SPACE:
                Actor currentButton = screen.getControllableActors().get(currentButtonIndex);
                return touchButton(currentButton);
        }
        return false;
    }

    private boolean keyUpActor(int keycode) {
        switch (keycode) {
            case Input.Keys.ENTER:
            case Input.Keys.SPACE:
                Actor currentButton = screen.getControllableActors().get(currentButtonIndex);
                return releaseButton(currentButton);
        }

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    // Mouse touch down
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (screen.getControllableActors().size() != 0) {
            final Vector2 stageLoc = screen.getStage().screenToStageCoordinates(new Vector2(screenX, screenY));

            Actor hit = screen.getStage().hit(stageLoc.x, stageLoc.y, true);

            if (hit != null) {
                while (!(hit instanceof Button) && hit != null)
                    hit = hit.getParent();

                return hit != null && fireEventOn(hit, InputEvent.Type.touchDown) && fireEventOn(hit, InputEvent.Type.touchUp);
            } else {
                if (button == 0)
                    addKeyPress(MOUSE_LEFT, 0);
                else if (button == 1)
                    addKeyPress(MOUSE_RIGHT, 0);

                return ontouch(screenX, screenY, pointer, button);
            }
        } else {
            if (button == 0)
                addKeyPress(MOUSE_LEFT, 0);
            else if (button == 1)
                addKeyPress(MOUSE_RIGHT, 0);

            return ontouch(screenX, screenY, pointer, button);
        }
    }

    private boolean ontouch(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (button == 0) {
            input[0].remove(MOUSE_LEFT);
            return true;
        } else if (button == 1) {
            input[0].remove(MOUSE_RIGHT);
            return true;
        }

        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return onhover(screenX, screenY);
    }

    private boolean onhover(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    // Gesture listener touch down
    public boolean touchDown(float x, float y, int pointer, int button) {
        if (screen.getControllableActors().size() == 0) {
            if (button == 0)
                addKeyPress(MOUSE_LEFT, 0);
            else if (button == 1)
                addKeyPress(MOUSE_RIGHT, 0);
            return ontouch((int) x, (int) y, pointer, button);
        } else {
            return false; // Don't handle the event, let the stage do it
        }
    }

    public boolean tap(float screenX, float screenY, int count, int button) {
        if (screen.getControllableActors().size() == 0) {
            if (button == 0)
                addKeyPress(MOUSE_LEFT, 0);
            else if (button == 1)
                addKeyPress(MOUSE_RIGHT, 0);
            ontouch((int) screenX, (int) screenY, count, button);
            return false;       // Return false as we want the listener to also catch the up-event
        } else {
            return keyDownActor(Input.Keys.ENTER);
        }
    }

    public boolean longPress(float x, float y) {
        addKeyPress(MOUSE_RIGHT, 0);
        onlongpress(x, y);
        return true;
    }

    private void onlongpress(float x, float y) {

    }

    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return true;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return true;
    }

    public boolean zoom(float initialDistance, float distance) {
        return true;
    }

    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }

    public IntMap<Float> getInput(int controllerNum) {
        return input[controllerNum];
/*
        for (IntMap.Entry<Float> entry : entries) {
            System.out.printf("Input: %s %f\n", entry.key, entry.value);
        }
        System.out.println("-------------------------------");
*/
    }

    public static final class ControllerInput {
        public final int num;
        public float leftAxisX;
        public float leftAxisY;
        public float rightAxisX;
        public float rightAxisY;
        public boolean triggerLeft;
        public boolean triggerRight;
        public boolean buttonY;
        public boolean buttonA;
        public boolean buttonX;
        public boolean buttonB;
        public boolean buttonL1;
        public boolean buttonR1;
        public boolean buttonPovN;
        public boolean buttonPovE;
        public boolean buttonPovS;
        public boolean buttonPovW;
        public Controller controller;

        public ControllerInput(int num, Controller controller) {
            this.controller = controller;
            this.num = num;
        }
    }
}
