package no.dkit.gamelib.xxxx.core.game.view.table;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import no.dkit.gamelib.xxxx.core.XXXX;
import no.dkit.gamelib.xxxx.core.game.Config;

public class SettingsTable extends ControllableTable {

    private SelectBox<Graphics.DisplayMode> selectBox;
    private SelectBox<Integer> selectBox2;
    private SelectBox<Integer> selectBox3;
    private CheckBox fullscreen;
    private CheckBox vsync;
    private CheckBox crt;
    private CheckBox decorated;
    private CheckBox sound;
    private CheckBox music;
    private CheckBox keyboardMouse;

    public SettingsTable() {
        super(true);
        titleLabel.setText("Preferences");
        XXXX.loadSettings();
    }

    @Override
    protected Table getContentTable() {
        Table table = new Table(XXXX.skin);
        table.pad(25);
        int rowWidth = tableWidth - 50;

        final Graphics.Monitor primaryMonitor = Gdx.graphics.getPrimaryMonitor();
        final Graphics.DisplayMode currentDisplayMode = Gdx.graphics.getDisplayMode(primaryMonitor);
        final Graphics.DisplayMode[] displayModes = Gdx.graphics.getDisplayModes(primaryMonitor);

        selectBox = getDisplayModesDropdown(currentDisplayMode, displayModes);
        selectBox2 = getNumPlayersDropdown(XXXX.settings.getNumPlayers(), Config.MAX_NUMBER_OF_PLAYERS);
        selectBox3 = getNumRoundsDropdown();

        fullscreen = new CheckBox("Fullscreen", XXXX.skin);
        vsync = new CheckBox("V-Sync", XXXX.skin);
        crt = new CheckBox("CRT Effect", XXXX.skin);
        decorated = new CheckBox("Titlebar", XXXX.skin);
        sound = new CheckBox("Sound", XXXX.skin);
        music = new CheckBox("Music", XXXX.skin);
        keyboardMouse = new CheckBox("Player 1 uses keyboard/mouse", XXXX.skin);

        fullscreen.setChecked(XXXX.settings.isFullscreen());
        vsync.setChecked(XXXX.settings.isVsync());
        crt.setChecked(XXXX.settings.isCrt());
        decorated.setChecked(XXXX.settings.isFullscreen());
        sound.setChecked(XXXX.settings.isSound());
        music.setChecked(XXXX.settings.isMusic());
        keyboardMouse.setChecked(XXXX.settings.isKeyboardMouse());

        table.add("Player options", "large").width(rowWidth).align(Align.left).colspan(3);
        table.row();
        table.row();
        table.add("Number of players", "large").width(rowWidth / 3).align(Align.left);
        table.add();
        table.add(selectBox2).width(rowWidth / 3).align(Align.left);
        table.row();
        table.row();
        table.add(keyboardMouse).width(rowWidth).align(Align.left).colspan(3);
        table.row();
        table.row();
        table.add("Number of rounds", "large").width(rowWidth / 3).align(Align.left);
        table.add();
        table.add(selectBox3).width(rowWidth / 3).align(Align.left);
        table.row();
        table.row();
        table.add("Graphics options", "large").width(rowWidth).align(Align.left).colspan(3);
        table.row();
        table.row();
        table.add(selectBox).width(rowWidth).align(Align.left).colspan(3);
        table.row();
        table.row();
        table.add(fullscreen).width(rowWidth / 3).align(Align.left);
        table.add();
        //table.add(vsync).width(rowWidth / 3).align(Align.left);
        table.add(crt).width(rowWidth / 3).align(Align.left);
        table.row();
        table.row();
        table.add("Sound options", "large").width(rowWidth).align(Align.left).colspan(3);
        table.row();
        table.row();
        table.add(sound).width(rowWidth / 3).align(Align.left);
        table.add();
        table.add(music).width(rowWidth / 3).align(Align.left);

        //table.row();
        //table.add(decorated).width(tableWidth / 3).align(Align.left);

        return table;
    }

    @Override
    protected void onCloseClick() {
        XXXX.changeScreen(XXXX.SCREEN.STARTMENU);
    }

    @Override
    protected void onActionClick() {
        XXXX.settings.setScreenHeight(selectBox.getSelected().height);
        XXXX.settings.setScreenWidth(selectBox.getSelected().width);
        XXXX.settings.setFullscreen(fullscreen.isChecked());
        XXXX.settings.setVsync(vsync.isChecked());
        XXXX.settings.setDecorated(decorated.isChecked());
        XXXX.settings.setCrt(crt.isChecked());
        XXXX.settings.setSound(sound.isChecked());
        XXXX.settings.setMusic(music.isChecked());
        XXXX.settings.setKeyboardMouse(keyboardMouse.isChecked());
        XXXX.settings.setNumPlayers(selectBox2.getSelected());
        XXXX.settings.setNumRounds(selectBox3.getSelected());
        XXXX.saveSettings();

        if (XXXX.settings.isFullscreen()) {
            Gdx.graphics.setFullscreenMode(selectBox.getSelected());
        } else {
            Gdx.graphics.setWindowedMode(XXXX.settings.getScreenWidth(), XXXX.settings.getScreenHeight());
        }

        Gdx.graphics.setVSync(XXXX.settings.isVsync());
        //Gdx.graphics.setDecorated(XXXX.settings.isDecorated());    // TODO: DOES NOT WORK YET

        XXXX.changeScreen(XXXX.SCREEN.STARTMENU);
    }

    private SelectBox<Graphics.DisplayMode> getDisplayModesDropdown(Graphics.DisplayMode currentDisplayMode, Graphics.DisplayMode[] displayModes) {
        SelectBox<Graphics.DisplayMode> selectBox = new SelectBox<>(XXXX.skin, "default");
        selectBox.setItems(displayModes);

        selectBox.setSelected(currentDisplayMode);

        for (Graphics.DisplayMode displayMode : displayModes) {
            if (displayMode.width == XXXX.settings.getScreenWidth() && displayMode.height == XXXX.settings.getScreenHeight())
                selectBox.setSelected(displayMode);
        }

        return selectBox;
    }

    private SelectBox<Integer> getNumPlayersDropdown(Integer currentNumPlayers, Integer maxNumPlayers) {
        Array<Integer> playerNum = new Array<>(maxNumPlayers);

        for (int i = 2; i <= maxNumPlayers; i++)
            playerNum.add(i);

        SelectBox<Integer> selectBox = new SelectBox<>(XXXX.skin, "default");
        selectBox.setItems(playerNum);
        selectBox.setSelected(currentNumPlayers);

        return selectBox;
    }

    private SelectBox<Integer> getNumRoundsDropdown() {
        Array<Integer> roundsNum = new Array<>(4);
        roundsNum.add(1);
        roundsNum.add(3);
        roundsNum.add(5);
        roundsNum.add(10);

        SelectBox<Integer> selectBox = new SelectBox<>(XXXX.skin, "default");
        selectBox.setItems(roundsNum);
        selectBox.setSelected(XXXX.settings.getNumPlayers());

        return selectBox;
    }
}
