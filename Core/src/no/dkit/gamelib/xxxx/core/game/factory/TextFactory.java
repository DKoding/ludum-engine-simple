package no.dkit.gamelib.xxxx.core.game.factory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFontCache;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pools;
import no.dkit.gamelib.xxxx.core.game.Config;

public class TextFactory {
    public static final String FONT_NAME = "plump.ttf";

    BitmapFont font;
    BitmapFontCache fontCache;

    Array<TextItem> texts;
    static TextFactory instance = null;

    long lastUpdateCache;
    private GlyphLayout measure = new GlyphLayout();

    public static TextFactory getInstance() {
        if (instance == null)
            instance = new TextFactory();

        return instance;
    }

    private TextFactory() {
        texts = new Array<>();
        font = getFont(Config.TEXT_SIZE);
        font.setUseIntegerPositions(false);
        fontCache = new BitmapFontCache(font);
        fontCache.setUseIntegerPositions(false);
    }

    private void addText(TextItem textItem) {
        measure.setText(font, textItem.getText());
        textItem.updatePosition(textItem.getX() - measure.width / 2, textItem.getY() + measure.height / 2);
        texts.add(textItem);
        updateCache(textItem);
    }

    public void addText(String text) {
        addScreenText(text, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 4, Color.BLACK);
    }

    public void addMoreText(String text) {
        addScreenText(text, Gdx.graphics.getWidth() / 2, (Gdx.graphics.getHeight() / 4) + Config.TEXT_SIZE * 2, Color.YELLOW);
    }

    public void addEvenMoreText(String text) {
        addScreenText(text, Gdx.graphics.getWidth() / 2, (Gdx.graphics.getHeight() / 4) + Config.TEXT_SIZE * 4, Color.YELLOW);
    }

    public void addScreenText(String text, int x, int y) {
        addScreenText(text, x, y, Color.BLACK);
    }

    public void addText(String text, Vector2 position) {
        addWorldText(text, position.x, position.y, Color.WHITE);
    }

    public void addText(String text, Vector2 position, Color color) {
        addWorldText(text, position.x, position.y, color);
    }

    public void addWorldText(String text, float x, float y, Color color) {
        final TextItem obtain = obtainTextItem(text, color);
        obtain.setPosition(x, y);
        addText(obtain);
    }

    public void addScreenText(String text, int x, int y, Color color) {
        final TextItem obtain = obtainTextItem(text, color);
        obtain.setPosition(x, y);
        addText(obtain);
    }

    private TextItem obtainTextItem(String text, Color color) {
        final TextItem obtain = Pools.obtain(TextItem.class);
        obtain.setText(text);
        obtain.setColor(color);
        obtain.setAlpha(1);
        obtain.setStartTime(System.currentTimeMillis());
        return obtain;
    }

    public void updateCache(TextItem textItem) {
        lastUpdateCache = System.currentTimeMillis();

        for (TextItem text : texts) {
            if (textItem != null && text == textItem) continue;

            text.updatePosition(text.getX() + fontCache.getX(), text.getY() + fontCache.getY());
        }

        fontCache.clear();

        for (TextItem text : texts) {
            fontCache.setColor(text.getColor());
            fontCache.addText(text.getText(), text.getX(), text.getY());
        }
    }

    public void draw(SpriteBatch spriteBatch) {
        fontCache.draw(spriteBatch);
    }

    public void update(float deltaX, float deltaY) {
        long now = System.currentTimeMillis();

        boolean removePerformed = false;

        for (TextItem text : texts) {
            if (now > text.getStartTime() + Config.TEXT_TIME_TO_LIVE) {
                texts.removeValue(text, false);
                removePerformed = true;
            }
        }

        if (removePerformed)
            updateCache(null); // Update everything

        fontCache.translate(0, Config.TEXT_SCROLL_SPEED);
    }

    public void dispose() {
        font.dispose();
        instance = null;
    }

    public void clear() {
        texts.clear();
        fontCache.clear();
    }

    private BitmapFont getFont(int size) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(FONT_NAME));
        FreeTypeFontGenerator.FreeTypeFontParameter parameters = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameters.size = size;
        parameters.magFilter = Texture.TextureFilter.Linear;
        parameters.minFilter = Texture.TextureFilter.Linear;
        BitmapFont font = generator.generateFont(parameters);
        generator.dispose();
        return font;
    }

    public BitmapFont getFont() {
        return font;
    }
}
