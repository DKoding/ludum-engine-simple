package no.dkit.gamelib.xxxx.core.game.controller;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.equations.Bounce;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import no.dkit.gamelib.xxxx.core.XXXX;
import no.dkit.gamelib.xxxx.core.game.factory.EffectFactory;
import no.dkit.gamelib.xxxx.core.game.view.TweenActorAccessor;

import javax.naming.TimeLimitExceededException;

public class ImageListener extends ClickListener {

    private Vector2 coords = new Vector2();

    public interface ListenerActions {
        void onClick();

        void onEnter();

        void onExit();
    }

    public static class DefaultListener implements ListenerActions {
        @Override
        public void onClick() {

        }

        @Override
        public void onEnter() {

        }

        @Override
        public void onExit() {

        }
    }

    private final Image image;
    private final ListenerActions listener;

    public ImageListener(Image image, ListenerActions listener) {
        this.image = image;
        this.listener = listener;
    }

    @Override
    public void clicked(InputEvent event, float x, float y) {
        clickEffect(image);
        listener.onClick();
    }

    @Override
    public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
        enterEffect(image);
        listener.onEnter();
    }

    @Override
    public void exit(InputEvent event, float x, float y, int pointer, Actor fromActor) {
        exitEffect(image);
        listener.onExit();
    }

    public void clickEffect(Actor actor) {
        coords.set(actor.getOriginX(), actor.getOriginY());
        actor.localToStageCoordinates(coords);
        actor.getStage().stageToScreenCoordinates(coords);
        actor.setOrigin(Align.center);
        Tween.to(actor, TweenActorAccessor.SCALE_XY, .1f).target(1.2f, 1.2f).repeatYoyo(1, 0).start(XXXX.getTweener());
    }

    public void enterEffect(Actor actor) {
        actor.setOrigin(Align.center);
        Tween.to(actor, TweenActorAccessor.SCALE_XY, .5f).target(1.2f, 1.2f).ease(Bounce.OUT).start(XXXX.getTweener());
    }

    public void exitEffect(Actor actor) {
        actor.setOrigin(Align.center);
        Tween.to(actor, TweenActorAccessor.SCALE_XY, .1f).target(1f, 1f).start(XXXX.getTweener());
    }
}
