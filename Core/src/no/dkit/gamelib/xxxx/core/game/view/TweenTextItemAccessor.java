package no.dkit.gamelib.xxxx.core.game.view;

import aurelienribon.tweenengine.TweenAccessor;
import no.dkit.gamelib.xxxx.core.game.factory.TextItem;

public class TweenTextItemAccessor implements TweenAccessor<TextItem> {
    public static final int WORLD_POSITION_XY = 1;
    public static final int SCREEN_POSITION_XY = 2;
    public static final int ALPHA = 4;

    @Override
    public int getValues(TextItem target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case WORLD_POSITION_XY:
                returnValues[0] = target.getX();
                returnValues[1] = target.getY();
                return 2; // Number of values returned
            case SCREEN_POSITION_XY:
                returnValues[0] = target.getX();
                returnValues[1] = target.getY();
                return 2; // Number of values returned
            case ALPHA:
                returnValues[0] = target.getAlpha();
                return 1; // Number of values returned
            default:
                return -1;
        }
    }

    @Override
    public void setValues(TextItem target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case WORLD_POSITION_XY:
                target.setPosition(newValues[0], newValues[1]);
                break;
            case SCREEN_POSITION_XY:
                target.setPosition((int) newValues[0], (int) newValues[1]);
                break;
            case ALPHA:
                target.setAlpha(newValues[0]);
                break;
            default:
        }
    }
}
