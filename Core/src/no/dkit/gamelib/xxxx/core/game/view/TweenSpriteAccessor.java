package no.dkit.gamelib.xxxx.core.game.view;

import aurelienribon.tweenengine.TweenAccessor;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class TweenSpriteAccessor implements TweenAccessor<Sprite> {
    public static final int POSITION_XY = 3;
    public static final int SIZE_XY = 6;
    public static final int SCALE_XY = 7;
    public static final int ROTATION = 8;

    @Override
    public int getValues(Sprite target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case POSITION_XY:
                returnValues[0] = target.getX();
                returnValues[1] = target.getY();
                return 2; // Number of values returned
            case SIZE_XY:
                returnValues[0] = target.getWidth();
                returnValues[1] = target.getHeight();
                return 2; // Number of values returned
            case SCALE_XY:
                returnValues[0] = target.getScaleX();
                returnValues[1] = target.getScaleY();
                return 2; // Number of values returned
            case ROTATION:
                returnValues[0] = target.getRotation();
                return 1; // Number of values returned
            default:
                return -1;
        }
    }

    @Override
    public void setValues(Sprite target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case POSITION_XY:
                target.setPosition(newValues[0], newValues[1]);
                break;
            case SIZE_XY:
                target.setSize(newValues[0], newValues[1]);
                break;
            case SCALE_XY:
                target.setScale(newValues[0], newValues[1]);
                break;
            case ROTATION:
                target.setRotation(newValues[0]);
            default:
        }
    }
}
