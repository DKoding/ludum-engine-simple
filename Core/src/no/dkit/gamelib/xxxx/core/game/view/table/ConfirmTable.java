package no.dkit.gamelib.xxxx.core.game.view.table;

import no.dkit.gamelib.xxxx.core.XXXX;
import no.dkit.gamelib.xxxx.core.game.controller.GameScreen;

public abstract class ConfirmTable extends NoImageTable {
    public ConfirmTable(String question, String description) {
        super(true);
        titleLabel.setText(question);
        textLabel.setText(description);
    }

    @Override
    protected void onCloseClick() {
        ((GameScreen) XXXX.getGame().getScreen()).removeControllableActor(this);
    }
}
