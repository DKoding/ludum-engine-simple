package no.dkit.gamelib.xxxx.core;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import no.dkit.gamelib.xxxx.core.game.controller.AbstractScreen;
import no.dkit.gamelib.xxxx.core.game.controller.GameScreen;
import no.dkit.gamelib.xxxx.core.game.controller.LoadingScreen;
import no.dkit.gamelib.xxxx.core.game.controller.MenuScreen;
import no.dkit.gamelib.xxxx.core.game.controller.ResultsScreen;
import no.dkit.gamelib.xxxx.core.game.controller.SplashScreen;
import no.dkit.gamelib.xxxx.core.game.factory.EffectFactory;
import no.dkit.gamelib.xxxx.core.game.factory.ResourceFactory;
import no.dkit.gamelib.xxxx.core.game.factory.SoundFactory;
import no.dkit.gamelib.xxxx.core.game.factory.TextFactory;
import no.dkit.gamelib.xxxx.core.game.factory.TextItem;
import no.dkit.gamelib.xxxx.core.game.transition.ChangeCallback;
import no.dkit.gamelib.xxxx.core.game.transition.DoneCallback;
import no.dkit.gamelib.xxxx.core.game.transition.TransitionScreen;
import no.dkit.gamelib.xxxx.core.game.view.TweenActorAccessor;
import no.dkit.gamelib.xxxx.core.game.view.TweenCameraAccessor;
import no.dkit.gamelib.xxxx.core.game.view.TweenSpriteAccessor;
import no.dkit.gamelib.xxxx.core.game.view.TweenTextItemAccessor;

public class XXXX extends ApplicationAdapter {
    private static boolean isChanging = false;
    private static Game game;
    public static Skin skin;

    private static Screen existingGameScreen;
    public static Settings settings;
    public static Preferences preferences;

    public static boolean screenControls = false;

    public static int HIGHSCORE = 0;

    static TweenManager tweener;

    public static int numLevels;

    public enum GAME_STATE {INIT, STARTING, RUNNING, HIDDEN, RESUMING, FAILED, WON, PAUSING, CLICK_TO_CONTINUE}

    public static GAME_STATE gameState = GAME_STATE.INIT;

    public XXXX() {

    }

    public enum CONTROL_LAYOUT {UP_DOWN_FIRE, JOYSTICK_FIRE, RIGHT_LEFT_FIRE}

    public static CONTROL_LAYOUT controlType = CONTROL_LAYOUT.UP_DOWN_FIRE;

    public static Game getGame() {
        return game;
    }

    public static void setScreen(Screen screen) {
        game.setScreen(screen);
        isChanging = false;
    }

    public enum SCREEN {SPLASH, STARTMENU, GAME, WIN_LEVEL, RESULTS, NEWLEVEL, INTRO, EXTRO}

    public enum AIM_MODE {DIRECTION, FOCUSED}

    public static AIM_MODE aimMode = AIM_MODE.DIRECTION;

    public void create() {
        if (game == null) {
            game = new Game() {
                @Override
                public void create() {
                    setupTweener();
                }
            };
            game.create();
            skin = new Skin(Gdx.files.getFileHandle("uiskin.json", Files.FileType.Internal));
        }

        preferences = Gdx.app.getPreferences("LD35.prefs");

        settings = new Settings();
        loadSettings();

        if (Gdx.app.getType() == Application.ApplicationType.Desktop) {
/*
            if (settings.isFullscreen()) {
                Graphics.DisplayMode primaryDesktopMode = Gdx.graphics.getDisplayMode();
                Gdx.graphics.setFullscreenMode(primaryDesktopMode);
            } else {
*/
            Gdx.graphics.setWindowedMode(1280, 720);
/*
            }
*/

            Gdx.graphics.setVSync(settings.isVsync());
        }

        game.setScreen(new SplashScreen());
    }

    private void setupTweener() {
        tweener = new TweenManager();
        Tween.registerAccessor(OrthographicCamera.class, new TweenCameraAccessor());
        Tween.registerAccessor(Actor.class, new TweenActorAccessor());
        Tween.registerAccessor(Sprite.class, new TweenSpriteAccessor());
        Tween.registerAccessor(TextItem.class, new TweenTextItemAccessor());
    }

    public static TweenManager getTweener() {
        return tweener;
    }

    @Override
    /**
     * The Main Loop
     */
    public void render() {
        if (game != null && game.getScreen() != null)
            game.render();
    }

    @Override
    public void dispose() { // TODO - call dispose statically on Factories to avoid error when disposing factory not created yet
        ResourceFactory.getInstance().dispose();
        TextFactory.getInstance().dispose();
        EffectFactory.getInstance().dispose();
        SoundFactory.getInstance().dispose();

        tweener = null;
        skin.dispose();
        game.dispose();
    }

    public static void changeScreen(SCREEN to) {
        if (isChanging) return;

        isChanging = true;

        final Screen oldScreen = game.getScreen();

        if (oldScreen instanceof AbstractScreen) {
            Controllers.removeListener(((AbstractScreen) oldScreen).getListener());
            XXXX.getTweener().killAll();
            ((AbstractScreen) oldScreen).cancelAllTasks();
        }

        Screen newScreen = null;

        if (to.equals(SCREEN.STARTMENU)) {
            newScreen = new MenuScreen();
        } else if (to.equals(SCREEN.STARTMENU)) {
            newScreen = new MenuScreen();
        } else if (to.equals(SCREEN.NEWLEVEL)) {
            newScreen = new LoadingScreen(to);
        } else if (to.equals(SCREEN.GAME)) {
            newScreen = new GameScreen();
        } else if (to.equals(SCREEN.RESULTS)) {
            newScreen = new ResultsScreen();
        }

        game.setScreen(new TransitionScreen(oldScreen, newScreen,
                new ChangeCallback() {
                    @Override
                    public void onSwitch(Screen oldScreen, Screen newScreen) {
                        if (newScreen instanceof LoadingScreen)
                            ((LoadingScreen) newScreen).onSwitch(oldScreen, newScreen);
                    }
                },
                new DoneCallback() {
                    public void done(Screen oldScreen, Screen newScreen) {
                        isChanging = false;
                        gameState = GAME_STATE.INIT;
                        game.setScreen(newScreen);
                    }
                }));
    }

    public static void saveSettings() {
        preferences.putBoolean("vsync", settings.isVsync());
        preferences.putBoolean("fullscreen", settings.isFullscreen());
        preferences.putInteger("screenwidth", settings.getScreenWidth());
        preferences.putInteger("screenheight", settings.getScreenHeight());
        preferences.putInteger("bpp", settings.getBitsPerPixel());
        preferences.putInteger("refresh", settings.getRefreshRate());
        preferences.putBoolean("decorated", settings.isDecorated());
        preferences.putBoolean("crt", settings.isCrt());
        preferences.putBoolean("sound", settings.isSound());
        preferences.putBoolean("music", settings.isMusic());
        preferences.putBoolean("keymouse", settings.isKeyboardMouse());
        preferences.putInteger("numplayers", settings.getNumPlayers());
        preferences.putInteger("numrounds", settings.getNumRounds());
        preferences.flush();
    }

    public static void loadSettings() {
        XXXX.settings.setFullscreen(preferences.getBoolean("fullscreen", false));
        XXXX.settings.setVsync(preferences.getBoolean("vsync", true));
        XXXX.settings.setScreenWidth(preferences.getInteger("screenwidth", 640));
        XXXX.settings.setScreenHeight(preferences.getInteger("screenheight", 480));
        XXXX.settings.setBitsPerPixel(preferences.getInteger("bpp", 16));
        XXXX.settings.setRefreshRate(preferences.getInteger("refresh", 60));
        XXXX.settings.setDecorated(preferences.getBoolean("decorated", true));
        XXXX.settings.setCrt(preferences.getBoolean("crt", true));
        XXXX.settings.setSound(preferences.getBoolean("sound", true));
        XXXX.settings.setMusic(preferences.getBoolean("music", true));
        XXXX.settings.setKeyboardMouse(preferences.getBoolean("keymouse", true));
        XXXX.settings.setNumPlayers(preferences.getInteger("numplayers", 3));
        XXXX.settings.setNumRounds(preferences.getInteger("numrounds", 1));
    }

    public static class Settings {
        int numPlayers;
        int numRounds;
        int screenWidth;
        int screenHeight;
        int refreshRate;
        int bitsPerPixel;
        boolean fullscreen;
        boolean vsync;
        boolean decorated;
        boolean crt;
        boolean sound;
        boolean music;
        boolean keyboardMouse;

        public Settings() {
        }

        public int getScreenWidth() {
            return screenWidth;
        }

        public void setScreenWidth(int screenWidth) {
            this.screenWidth = screenWidth;
        }

        public int getScreenHeight() {
            return screenHeight;
        }

        public void setScreenHeight(int screenHeight) {
            this.screenHeight = screenHeight;
        }

        public boolean isFullscreen() {
            return fullscreen;
        }

        public void setFullscreen(boolean fullscreen) {
            this.fullscreen = fullscreen;
        }

        public boolean isVsync() {
            return vsync;
        }

        public void setVsync(boolean vsync) {
            this.vsync = vsync;
        }

        public boolean isDecorated() {
            return decorated;
        }

        public void setDecorated(boolean decorated) {
            this.decorated = decorated;
        }

        public boolean isCrt() {
            return crt;
        }

        public void setCrt(boolean crt) {
            this.crt = crt;
        }

        public boolean isSound() {
            return sound;
        }

        public void setSound(boolean sound) {
            this.sound = sound;
        }

        public boolean isMusic() {
            return music;
        }

        public void setMusic(boolean music) {
            this.music = music;
        }

        public boolean isKeyboardMouse() {
            return keyboardMouse;
        }

        public void setKeyboardMouse(boolean keyboardMouse) {
            this.keyboardMouse = keyboardMouse;
        }

        public int getNumPlayers() {
            return numPlayers;
        }

        public void setNumPlayers(int numPlayers) {
            this.numPlayers = numPlayers;
        }

        public int getRefreshRate() {
            return refreshRate;
        }

        public void setRefreshRate(int refreshRate) {
            this.refreshRate = refreshRate;
        }

        public int getBitsPerPixel() {
            return bitsPerPixel;
        }

        public void setBitsPerPixel(int bitsPerPixel) {
            this.bitsPerPixel = bitsPerPixel;
        }

        public int getNumRounds() {
            return numRounds;
        }

        public void setNumRounds(int numRounds) {
            this.numRounds = numRounds;
        }
    }
}

