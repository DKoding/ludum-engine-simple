package no.dkit.gamelib.xxxx.core.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;

// TODO: WIP
public final class ScreenUtil {
    final static int potW = MathUtils.nextPowerOfTwo(Gdx.graphics.getBackBufferWidth());
    final static int potH = MathUtils.nextPowerOfTwo(Gdx.graphics.getBackBufferHeight());
    final static Pixmap potPixmap = new Pixmap(potW, potH, Format.RGB888);
    final static Texture texture = new Texture(potPixmap);

    private static long lastUpdate;

    private static TextureRegion textureRegion =
            new TextureRegion(texture, 0, Gdx.graphics.getBackBufferHeight(),
                    Gdx.graphics.getBackBufferWidth(), -Gdx.graphics.getBackBufferHeight()); // Flipping
    private static Pixmap pixmap;

    public static TextureRegion getFrameBufferTexture() {
        if(System.currentTimeMillis() > lastUpdate + 100) {
            potPixmap.drawPixmap(getFrameBufferPixmap(0, 0, Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight()), 0, 0);
            texture.draw(potPixmap, 0, 0);
            lastUpdate = System.currentTimeMillis();
        }
        return textureRegion;
    }

    public static Pixmap getFrameBufferPixmap(int x, int y, int w, int h) {
        Gdx.gl.glPixelStorei(GL20.GL_PACK_ALIGNMENT, 1);
        if (pixmap == null)
            pixmap = new Pixmap(w, h, Format.RGB888);
        Gdx.gl.glReadPixels(x, y, w, h, GL20.GL_RGB, GL20.GL_UNSIGNED_BYTE, pixmap.getPixels());
        return pixmap;
    }
}
