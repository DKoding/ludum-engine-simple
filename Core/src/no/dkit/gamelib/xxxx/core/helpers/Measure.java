package no.dkit.gamelib.xxxx.core.helpers;

import no.dkit.gamelib.xxxx.core.game.Config;

public class Measure {
    private long start = 0;
    private int measureCount = 0;
    private int measureSum = 0;
    private int interval = 0;
    private float average = 0;

    private String name;

    public Measure(String name) {
        this.name = name;
        this.interval = 60;
    }

    public Measure(String name, int interval) {
        this.name = name;
        this.interval = interval;
    }

    public void start() {
        measureCount++;
        start = System.currentTimeMillis();
    }

    public void end() {
        measureSum += System.currentTimeMillis() - start;

        average = (float) measureSum / (float) measureCount;

        if (measureCount == 100 && average > Config.DEGUG_THRESHOLD_MILLIS) {
            System.out.println(name + ": " + average);

            measureSum = 0;
            measureCount = 0;
        }
    }
}
