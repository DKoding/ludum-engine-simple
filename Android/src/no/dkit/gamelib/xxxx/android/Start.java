package no.dkit.gamelib.xxxx.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import no.dkit.gamelib.android.R;

public class Start extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Button button = (Button) findViewById(R.id.Button);
        button.setOnClickListener(new Button.OnClickListener() {

            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), AndroidStarter.class));
            }
        });
    }
}
