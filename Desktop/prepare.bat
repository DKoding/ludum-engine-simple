echo Did you do :desktop:dist first?
echo REMEMBER TO RUN IN BASH shell!
rm -rf build/obfuscatesource.jar
rm -rf build/desktop-1.0-obfuscated.jar
rm -rf build/tmp
mkdir build/tmp
cp build/libs/desktop-1.0.jar build/tmp
cd build/tmp
jar xfv desktop-1.0.jar
rm desktop-1.0.jar
cd ..
jar cmf tmp/META-INF/MANIFEST.MF obfuscatesource.jar -C tmp .