-injars       ../build/obfuscatesource.jar
-outjars      ../build/desktop-1.0-obfuscated.jar
-libraryjars  c:/Program Files/Java/jdk1.7.0_55/jre/lib/rt.jar

-dontoptimize
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
#-dontpreverify STUPID STUPID MAN YOU TRICKED ME   - Important to preverify with Java 1.7!
-verbose
-dontshrink
-keepattributes SourceFile,LineNumberTable

-printmapping mapping.txt
-keeppackagenames java.**,java.security.**,java.lang.reflect.**

-verbose
-dontnote
#-dontwarn de.matthiasmann.**
-dontwarn com.esotericsoftware.**
-dontwarn javax.xml.**
-dontwarn box2dLight.**
-dontwarn shaders.**
#-dontwarn moo.**
-dontwarn org.xmlpull.**
-dontwarn javazoom.jl.**
-dontwarn com.sun.**
-dontwarn org.json.**
-dontwarn org.apache.**
#-dontwarn org.objectweb.**
#-dontwarn org.objenesis.**
#-dontwarn com.jcraft.**
-dontwarn com.badlogic.**
-dontwarn org.lwjgl.**

-keepclassmembers class * { *** getPointer(); }

-keepclassmembers class com.badlogic.gdx.physics.box2d.World {
   boolean contactFilter(long, long);
   void    beginContact(long);
   void    endContact(long);
   void    preSolve(long, long);
   void    postSolve(long, long);
   boolean reportFixture(long);
   float   reportRayFixture(long, float, float, float, float, float);
}

-keep class org.lwjgl.** { *; }
-keep class com.badlogic.** { *; }
-keep class * implements com.badlogic.gdx.utils.Json*
-keep class box2dLight

-keepclassmembers class com.badlogic.gdx.physics.box2d.World {
   boolean contactFilter(long, long);
   void    beginContact(long);
   void    endContact(long);
   void    preSolve(long, long);
   void    postSolve(long, long);
   boolean reportFixture(long);
   float   reportRayFixture(long, float, float, float, float, float);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep public class no.dkit.gamelib.xxxx.desktop.XXXXDesktop { *; }

-keep public class java.lang.** { *; }

-keepclasseswithmembernames class java.lang.* {
    native <methods>;
}

-keepclasseswithmembernames class * {
  native <methods>;
}

-keepclassmembers,allowoptimization enum * {
  public static **[] values();
  public static ** valueOf(java.lang.String);
}
