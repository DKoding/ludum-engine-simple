package no.dkit.gamelib.xxxx.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3WindowListener;

public class XXXXWindowListener implements Lwjgl3WindowListener {
    private boolean isStuffUnsaved;

    @Override
    public void iconified() {
        // the window is not visible anymore, potentially stop background
        // work, mute audio, etc.
    }

    @Override
    public void deiconified() {
        // the window is visible again, start-up background work, unmute
        // audio, etc.
    }

    @Override
    public void focusLost() {
        // the window lost focus, pause the game
    }

    @Override
    public void focusGained() {
        // the window received input focus, unpause the game
    }

    @Override
    public boolean closeRequested() {
        // if there's unsaved stuff, we may not want to close
        // the window, but ask the user to save her work
        if (isStuffUnsaved) {
            // tell our app listener to show a save dialog
            return false;
        } else {
            // OK, the window may close
            return true;
        }
    }

    @Override
    public void filesDropped(String[] files) {

    }
}
