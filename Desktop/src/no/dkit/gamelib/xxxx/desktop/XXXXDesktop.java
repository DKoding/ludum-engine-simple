package no.dkit.gamelib.xxxx.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import no.dkit.gamelib.xxxx.core.XXXX;

public class XXXXDesktop {
    public static void main(String[] args) {
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setTitle("LD35 - Theme 'SHAPE SHIFT' - Shapematch Beat Jam");
        cfg.setResizable(false);
        cfg.setBackBufferConfig(8, 8, 8, 8, 16, 1, 0);
        cfg.setDecorated(true);
        cfg.setWindowListener(new XXXXWindowListener());
        cfg.setWindowedMode(800, 600);

        final XXXX listener = new XXXX();
        new Lwjgl3Application(listener, cfg);
    }
}
